<?php /* Template Name: Proposal Template */ ?>

<?php get_header() ?>

<script>
document.querySelector('header').style.display = 'none'
</script>

	<article class="bg-theme-1 position-relative">
		<style>
			body {
				padding-top: 0;
			}
		</style>
		<div class="container py-4 py-md-5">
			<div class="row pb-md-5 pb-lg-0">
				<div class="col-md order-md-2 index-1">
					<div class="d-flex justify-content-center justify-content-md-start">
						<a class="navbar-brand d-flex align-items-center text-break ws-init lh-1 mr-0 mt-md-3 mb-4" href="<?= get_site_url() ?>">
							<img width="40" height="40" src="<?= get_template_directory_uri() ?>/assets/img/logo-gajihub-light.svg" alt="Logo">
							<span class="sr-only">GajiHub</span>
						</a>
					</div>
					<h1 class="fs-1 text-white text-break text-center text-md-left mb-0">
						Kenali Fitur Kledo Secara Lengkap dalam 1 Menit
					</h1>
					<p class="fs-6 opacity-7 text-white text-break text-center text-md-left mt-2 mb-0">
						Kesulitan dalam mengelola pembukuan dan proses operasional bisnis? Mari mengenal lebih jauh fitur terbaik dari Kledo dan cari tahu apa yang bisa Kledo lakukan untuk membuat bisnis Anda menjadi lebih baik.
					</p>
					<div class="text-center text-md-left my-4">
						<a class="btn btn-warning rounded-pill text-uppercase" role="button" download="Proposal Kledo" href="#">
							<div class="d-flex align-items-center">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="32" height="32">
									<path fill="#292D32" opacity=".25" d="M22 10.7599C22 10.2899 21.62 9.8999 21.14 9.8999H2.86C2.38 9.8999 2 10.2799 2 10.7599C2 16.6499 6.11 20.7599 12 20.7599C17.89 20.7599 22 16.6399 22 10.7599Z"/>
									<path fill="#fff" d="M12.4598 15.9L15.3098 13.06C15.5998 12.77 15.5998 12.29 15.3098 12C15.0198 11.71 14.5398 11.71 14.2498 12L12.6898 13.56V3.98999C12.6898 3.57999 12.3498 3.23999 11.9398 3.23999C11.5298 3.23999 11.1898 3.57999 11.1898 3.98999V13.56L9.61984 12C9.32984 11.71 8.84984 11.71 8.55984 12C8.40984 12.15 8.33984 12.34 8.33984 12.53C8.33984 12.72 8.40984 12.91 8.55984 13.06L11.4098 15.9C11.6998 16.2 12.1698 16.2 12.4598 15.9Z"/>
								</svg>
								<span class="fs-7 mx-2">
									Unduh <b>Proposal</b>
								</span>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-6 col-lg-5 order-md-1 index-1 text-center">
					<img class="img-fluid ml-n4 ml-md-0" width="400" height="600" src="https://kledo.com/landing/assets/proposal.png">
				</div>
			</div>
		</div>
		<div class="position-absolute left right" style="bottom:150px">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-bottom">
				<polygon points="0,0 100,25 100,30 0,30"/>
			</svg>
		</div>
		<div class="position-absolute left right bg-white overlay-bottom" style="height:155px"></div>
	</article>

	<article>
		<div class="container mb-5 mt-md-n5">
			<div class="row">
				<div class="col-md offset-md-6 offset-lg-5 mt-md-n5">
					<h2 class="fs-2 text-break text-center text-md-left mb-0">
						Platform yang tepat untuk membuat bisnis menjadi lebih hebat
					</h2>
					<p class="fs-6 opacity-7 text-break text-center text-md-left mt-2 mb-0">
						Efisiensi dan adaptasi dalam bisnis adalah langkah terpenting untuk membuat proses yang berkelanjutan. Dengan mengetahui semua fitur terbaik di software akuntansi Kledo, Anda telah selangkah lebih maju untuk membuat seluruh proses operasi bisnis berjalan lebih optimal.
					</p>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="container py-4 text-center text-md-left">
			<div class="row">
				<div class="col-md-6">
					<div class="small mb-3 mb-md-0" id="copyright">
						<span class="opacity-8">&copy; 2021</span> <a class="text-decoration-none" href="index.html">GajiHub</a> <span class="opacity-8">by Kledo. Developed with </span>
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20" height="20" fill="var(--theme-1)">
							<path d="M18.8008 9.91016C17.6708 9.91016 16.6608 10.4602 16.0308 11.3002C15.4008 10.4602 14.3908 9.91016 13.2608 9.91016C11.3508 9.91016 9.80078 11.4702 9.80078 13.3902C9.80078 14.1302 9.92078 14.8202 10.1208 15.4502C11.1008 18.5602 14.1408 20.4302 15.6408 20.9402C15.8508 21.0102 16.2008 21.0102 16.4108 20.9402C17.9108 20.4302 20.9508 18.5702 21.9308 15.4502C22.1408 14.8102 22.2508 14.1302 22.2508 13.3902C22.2608 11.4702 20.7108 9.91016 18.8008 9.91016Z"/>
							<path d="M20.75 8.34156C20.75 8.57156 20.52 8.72156 20.3 8.66156C18.95 8.31156 17.47 8.60156 16.35 9.40156C16.13 9.56156 15.83 9.56156 15.62 9.40156C14.83 8.82156 13.87 8.50156 12.86 8.50156C10.28 8.50156 8.18 10.6116 8.18 13.2116C8.18 16.0316 9.53 18.1416 10.89 19.5516C10.96 19.6216 10.9 19.7416 10.81 19.7016C8.08 18.7716 2 14.9116 2 8.34156C2 5.44156 4.33 3.10156 7.21 3.10156C8.92 3.10156 10.43 3.92156 11.38 5.19156C12.34 3.92156 13.85 3.10156 15.55 3.10156C18.42 3.10156 20.75 5.44156 20.75 8.34156Z"/>
						</svg>
					</div>
				</div>
				<div class="col-md-6 text-md-right">
					<div class="small">
						<a class="text-decoration-none" href="term.html">Syarat & Ketentuan</a>
						<span class="opacity-3 mx-2">|</span>
						<a class="text-decoration-none" href="privacy.html">Kebijakan Privasi</a>
					</div>
				</div>
			</div>
		</div>
	</article>

<?php get_footer() ?>