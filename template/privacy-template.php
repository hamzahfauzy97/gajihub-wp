<?php /* Template Name: Privacy Template */ ?>

<?php get_header() ?>

	<article id="header" class="bg-theme-1 text-white pb-md-4 pb-lg-5 pt-lg-3">
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						Kebijakan Privasi Kledo
					</h1>
				</div>
			</div>
		</div>
	</article>

	<article>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1)" class="overlay-top overlay-flip-y">
			<polygon points="100,30 0,30 0,25 100,0"/>
		</svg>
		<div class="container py-5">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto post-content">
					<p>This privacy policy ("<strong>Privacy Policy</strong>") provides our policies and procedures for collecting, transferring, processing, storing, using, and disclosing your information. Users ("<strong>you</strong>" or "<strong>your</strong>") can access the services provided by Kledo ("<strong>Kledo</strong>", "<strong>we</strong>" or "<strong>our</strong>") through our website and related applications and tools (the "<strong>Services</strong>"), and this Privacy Policy governs your access to the Services, regardless of what type of device or application you use to access them. By using our Services, you consent to the collection, transfer, processing, storage, disclosure and other uses of information described in this Privacy Policy. The term "<strong>Information</strong>" when used without other modifiers refers to all the different forms of data, content, and information described below.</p>

					<br>

					<h2 class="fs-2">THE INFORMATION WE COLLECT AND STORE</h2>

					<p>We may collect and store the following Information when you are using the Services:</p>

					<h3 class="fs-3"><strong>Information You Provide</strong></h3>
					<p>When you register for, or access, a Kledo account, or otherwise access the Services, we may collect some personal Information that can be used to contact or identify you ("<strong>Personal Information</strong>"), such as your name, phone number, email address, and home and business postal addresses. Personal Information may also include records of purchases and reservations you have made and contacts with Kledo. We do not store any credit card or other payment information from you.</p>

					<h3 class="fs-4"><strong>Files</strong></h3>
					<p>We collect and store the files you upload, download, or access with the Services ("Files"). If you add a File to a Kledo account that has been previously uploaded by you or another user, we may associate all or a portion of the previous File with your account rather than storing a duplicate.</p>

					<h3 class="fs-4"><strong>Performance Cookies</strong></h3>
					<p>These cookies allow us to count visits and traffic sources so we can measure and improve the performance of our site. They help us to know which pages are the most and least popular and see how visitors move around the site.</p>
					<p>All information these cookies collect is aggregated and therefore anonymous. If you do not allow these cookies we will not know when you have visited our site, and will not be able to monitor its performance.</p>

					<h3 class="fs-4"><strong>Functional Cookies</strong></h3>
					<p>These cookies enable the website to provide enhanced functionality and personalisation. They may be set by us or by third party providers whose services we have added to our pages.</p>
					<p>If you do not allow these cookies then some or all of these services may not function properly.</p>

					<h3 class="fs-4"><strong>Marketing Cookies</strong></h3>
					<p>These cookies may be set through our site by our advertising partners. They may be used by those companies to build a profile of your interests and show you relevant adverts on other sites.</p>
					<p>They do not store directly personal information, but are based on uniquely identifying your browser and internet device. If you do not allow these cookies, you will experience less targeted advertising.</p>

					<h3 class="fs-4"><strong>Social Media Cookies</strong></h3>
					<p>These cookies are set by a range of social media services that we have added to the site to enable you to share our content with your friends and networks. They are capable of tracking your browser across other sites and building up a profile of your interests. This may impact the content and messages you see on other websites you visit.</p>
					<p>If you do not allow these cookies you may not be able to use or see these sharing tools.</p>

					<h3 class="fs-4"><strong>Unclassified Cookies</strong></h3>
					<p>Unclassified cookies are cookies that we are in the process of classifying, together with the providers of individual cookies.</p>

					<h3 class="fs-4"><strong>Log Data</strong></h3>
					<p>When you use the Services, we automatically record Information from the device you use, its software, and your activity using the Services. This may include the device’s Internet Protocol ("<strong>IP</strong>") address, browser type, the web page(s) visited before you came to our website, Information you search for on our website, locale preferences, identification numbers associated with your devices, your mobile carrier, date and time stamps associated with transactions, system configuration, metadata concerning your Files, and other interactions with the Services.</p>

					<h3 class="fs-4"><strong>Integrations</strong></h3>
					<p>Kledo may allow third party integrators ("<strong>Integrators</strong>") to create applications and/or tools that supplement or enhance the Services (the "<strong>Integrations</strong>"). If you choose to access any such Integrations, the Integrators will access (via API) and use information you provide solely for the purpose of supplementing or enhancing the Services through the Integrations.</p>

					<br>

					<h2 class="fs-2">HOW WE USE YOUR INFORMATION</h2>

					<h3 class="fs-4"><strong>Personal Information</strong></h3>
					<p>Personal Information and data collected from cookies and other logging data may be used: (i) to provide and improve our Services and customer service, (ii) to administer your use of the Services, (iii) to better understand your needs and interests, (iv) to personalize and improve your experience, and (v) to provide or offer software updates and product announcements. If you no longer wish to receive communications from us, please follow the "unsubscribe" instructions provided in any of those communications, or update your account settings, as applicable. We do not sell your Personal Information, Files or any other information about you to third parties.</p>

					<h3 class="fs-4"><strong>Files</strong></h3>
					<p>Files are encrypted and stored with Amazon’s (AWS) storage service and may be accessed and downloaded by you. Kledo does not access or use your Files other than (i) in an encrypted form or (ii) in aggregated reports that do not contain, nor that can be used to extract, personally identifiable information.</p>

					<h3 class="fs-4"><strong>Analytics</strong></h3>
					<p>We may also collect some Information (ourselves or by using third party services) using logging and cookies, such as IP addresses, which can sometimes be correlated with Personal Information. We use this Information for the above purposes and to monitor and analyze use of the Services, for the Services’ technical administration, to increase our Services’ functionality and user-friendliness, and to verify users have the authorization needed for the Services to process their requests.</p>

					<br>

					<h2 class="fs-2">INFORMATION SHARING AND DISCLOSURE</h2>

					<h3 class="fs-4"><strong>Your Use</strong></h3>
					<p>We will display your Personal Information in your profile page according to the preferences you set in your profile or in any Kledo account which you access. You can review and revise your profile Information at any time.</p>

					<h3 class="fs-4"><strong>Service Providers, Business Partners and Others</strong></h3>
					<p>We do not sell your Personal Information, Files or other information about you to third parties. We may use certain trusted third-party companies and individuals to help us provide, analyze, and improve the Services (including but not limited to data storage, maintenance services, database management, web analytics, payment processing, and improvement of the Services’ features). These third parties may have access to your Information only for purposes of performing these limited tasks on our behalf and under obligations similar to those in our Privacy Policy. We use industry standard storage service to store your Files and other Information.</p>

					<h3 class="fs-4"><strong>Compliance with Laws and Law Enforcement Requests; Protection of Kledo’ Rights</strong></h3>
					<p>We may disclose to outside parties Files and Personal Information stored in Kledo accounts which you access and Information about you that we collect when we have a good faith belief that disclosure is reasonably necessary to (a) comply with a law, regulation or compulsory legal request; (b) protect the safety of any person from death or serious bodily injury; (c) prevent fraud or abuse of Kledo, its Services or its users; or (d) to protect Kledo’s property rights. If we provide your Files to a law enforcement agency as set forth above, we will remove Kledo’s encryption from the Files before providing them to law enforcement. However, Kledo will not be able to access and decrypt any Files on your behalf or that you encrypted prior to storing them via the Services.</p>

					<h3 class="fs-4"><strong>Transfers of Our Business</strong></h3>
					<p>If we are involved in a merger, acquisition, or sale of all or a portion of our assets, your Information may be transferred as part of that transaction, but we will notify you (for example, via email and/or a prominent notice on our website) of any change in control or use of your Personal Information or Files, or if such Information becomes subject to a different privacy policy.</p>

					<h3 class="fs-4"><strong>Non-private or Non-Personal Information</strong></h3>
					<p>We may, at our discretion, disclose your non-private, aggregated, or otherwise non-personal Information, such as usage statistics of our Services.</p>

					<h3 class="fs-4"><strong>Opting Out</strong></h3>
					<p>Other than the primary elements of the Services that we encrypt and provide through Amazon’s storage, you may opt out of our using or sharing any other Personal Information with our third-party business partners. Opting out may affect how well we are able to service your account(s) and customer needs, but we will still be able to provide you with the Services.</p>

					<h3 class="fs-4"><strong>Changing or Deleting Your Information</strong></h3>
					<p>If you are registered as the account holder, you may review, update, correct or delete the Personal Information provided in your registration or account profile by changing the "account settings." In some cases, we may retain copies of your Information and Files if required by law. For questions about your Personal Information or Files on our Services, please contact&nbsp;hello@kledo.com. We will respond to your inquiry within 30 days.</p>

					<h3 class="fs-4"><strong>Data Retention</strong></h3>
					<p>We may retain and use your Information as necessary to comply with our legal obligations, resolve disputes, and enforce our agreements. Consistent with these requirements, we will attempt to delete your Information quickly upon request. Please note, however, that there might be latency in deleting Information from our servers and backed-up versions might exist after deletion. In addition, we do not delete from our servers Files that you have in common with other users.</p>

					<h3 class="fs-4"><strong>Account Access</strong></h3>
					<p>You have the ability to:</p>
					<ol>
						<li>access, add, delete modify or save Files and other Information in and about your Kledo account;</li>
						<li>disclose, restrict, or access Information that you have provided or that is made available to you when using such Kledo account; and</li>
						<li>control how your Kledo account may be accessed, modified or deleted.</li>
					</ol>
					<p>Please refer to your organization’s policies if you have questions about your rights.</p>

					<h3 class="fs-4"><strong>Security</strong></h3>
					<p>The security of your Information is important to us. When you enter sensitive Information (such as a social security number) on our order forms and when you add or upload Files, we encrypt the transmission of that Information using secure socket layer technology (SSL).</p>
					<p>We follow generally accepted standards to protect the Information submitted to us, both during transmission and once we receive it. No method of electronic transmission or storage is 100% secure, however. Therefore, we cannot guarantee its absolute security. If you have any questions about security on our website, you can contact us at hello@kledo.com.</p>

					<h3 class="fs-4"><strong>Provisions Related to Users Residing in Europe</strong></h3>
					<p>Kledo complies with the provisions of the General Data Protection Regulation ("<strong>GDPR</strong>") made effective in the EU on May 25, 2018, and we are pursuing certification under the EU and Swiss Privacy Shield protocols. We are committed to protecting the security of your personal information, and we take commercially reasonable technical and organizational measures that are designed to that end.</p>
					<p>If you wish to opt out of any disclosures of your information to third parties or to prevent the use of your personal information for a purpose that is materially different from the purpose for which it was originally collected, you may log into your account and make changes necessary. The use of your information can be limited, or the information can be corrected, deleted, or exported to you or a third-party of your choice, except as required by law as indicated above.</p>

					<h3 class="fs-4"><strong>Our Policy Toward Children</strong></h3>
					<p>Our Services are not directed to persons under 13. We do not knowingly collect Personal Information from children under 13. If a parent or guardian becomes aware that his or her child has provided us with Personal Information without their consent, he or she should contact us at hello@kledo.com. If we become aware that a child under 13 has provided us with Personal Information, we will take steps to delete such Information.</p>

					<h3 class="fs-4"><strong>Contacting Us</strong></h3>
					<p>If you have any questions about this Privacy Policy, please contact us at hello@kledo.com.</p>

					<h3 class="fs-4"><strong>Changes to our Privacy Policy</strong></h3>
					<p>This Privacy Policy may change from time to time. If we make a change to this Privacy Policy that we believe materially reduces your rights, we will provide you with notice by email or by posting such changes on our site. We may provide notice of changes in other circumstances as well. By continuing to use the Services after those changes become effective, you agree to be bound by the revised Privacy Policy.</p>
					<br>
					<style>
						.post-content > *:not(h2):not(h3):not(h4) {
							text-align: justify;
							word-break: break-word;
							word-wrap: break-word;
							line-height: 2;
						}
						.post-content h2 {
							font-size: calc(1.4rem + .9vw);
						}
						.post-content h3 {
							font-size: calc(1.3rem + .6vw);
						}
						.post-content > h2 {
							text-align: left;
							margin-bottom: 20px;
						}
						.post-content > h2 ~ ol {
							margin-left: 7px;
						}
						.post-content ol {
							padding-inline-start: 20px;
							padding-left: 20px;
						}
						.post-content ol li li {
							list-style: lower-latin;
							margin-bottom: 0;
						}
						.post-content table td {
							vertical-align: top;
						}
					</style>
				</div>
			</div>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>