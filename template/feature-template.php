<?php /* Template Name: Feature Template */ ?>

<?php get_header() ?>

	<article id="header">
		<script>
			document.querySelectorAll('.navbar-dark').forEach(nav => {
				nav.classList.add('navbar-light')
				nav.classList.remove('navbar-dark')
			})
		</script>
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						Buat invoice lebih praktis
					</h1>
					<p class="fs-6 opacity-7 text-break text-center mt-2 mb-0 mx-md-5">
						Tinggalkan cara lama buat invoice. Lebih cepat, praktis dan dapatkan laporan otomatis!
					</p>
					<div class="text-center my-4">
						<a class="btn btn-danger rounded-pill py-2 pl-4 fs-7 text-uppercase ws-nowrap" href="#">
							<div class="d-flex align-items-center">
								<span class="ml-1 mr-2">
									Coba Gratis <b>Sekarang</b>
								</span>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40">
									<path fill="#292D32" opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
									<path fill="#fff" d="M16.03 11.4699L13.03 8.46994C12.74 8.17994 12.26 8.17994 11.97 8.46994C11.68 8.75994 11.68 9.23994 11.97 9.52994L13.69 11.2499H8.5C8.09 11.2499 7.75 11.5899 7.75 11.9999C7.75 12.4099 8.09 12.7499 8.5 12.7499H13.69L11.97 14.4699C11.68 14.7599 11.68 15.2399 11.97 15.5299C12.12 15.6799 12.31 15.7499 12.5 15.7499C12.69 15.7499 12.88 15.6799 13.03 15.5299L16.03 12.5299C16.32 12.2399 16.32 11.7599 16.03 11.4699Z"/>
								</svg>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article class="bg-theme-1">
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-top">
			<polygon points="0,0 0,5 50,30 100,5 100,0"/>
		</svg>
		<div class="container py-5">
			<div class="card-stack">
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/1-buat-invoice.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Buat invoice dengan desain elegan dan profesional</h3>
										<p class="fs-6 opacity-5 text-break">Cukup masukkan data dan Kledo akan membuatkan faktur dan invoice untuk pelangganmu.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/2-jenis-usaha-jasa-maupun.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Jenis usaha jasa maupun dagang, apapun bisa!</h3>
										<p class="fs-6 opacity-5 text-break">Apapun produk dan jasa yang kamu jual, pastikan buat invoice jadi lebih mudah menggunakan Kledo.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/3-laporan-penjualan-detail.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Laporan penjualan detail dan mendalam</h3>
										<p class="fs-6 opacity-5 text-break">Dapatkan laporan bisnis dan keuangan secara realtime. Ketahui produk apa yang paling laris, berapa pembayaran yang diterima tiap periode, berapa piutang dan lainnya. Segala hal tentang aspek bisnismu jadi sangat mudah dipantau.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/4-buat-penawaran-penjualan.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Buat penawaran penjualan & lacak statusnya</h3>
										<p class="fs-6 opacity-5 text-break">Kamu juga bisa membuat penawaran penjualan untuk pelangganmu dengan Kledo. Setelahnya status penawaran bisa kamu pantau dan lacak dari Kledo.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/5-buat-invoice-dari-penawaran.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Buat invoice dari penawaran hanya dengan satu klik</h3>
										<p class="fs-6 opacity-5 text-break">Tak perlu lagi input ulang untuk membuat invoice, cukup satu klik dari halaman penawaran, maka invoice mu langsung jadi.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/6-terima-pembayaran-invoice.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Terima pembayaran invoice bertahap</h3>
										<p class="fs-6 opacity-5 text-break">Pelangganmu bisa membayar invoice secara bertahap, dan Kledo sangat bisa mencatatnya dengan rapi dan benar.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/7-perhitungan-pajak-otomatis.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Perhitungan Pajak Otomatis</h3>
										<p class="fs-6 opacity-5 text-break">Semua pajak penjualan akan terrekap menjadi laporan pajak secara otomatis. Kamu bisa mengkustomisasi apapun jenis pajak yang kamu terapkan pada bisnismu.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/8-monitor-saldo-pelanggan.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Monitor Saldo Pelanggan</h3>
										<p class="fs-6 opacity-5 text-break">Monitor saldo pelangganmu jadi lebih mudah dengan Kledo. Ketahui pelanggan mana saja yang membeli lebih banyak, mana yang menunggak, dan mana yang bisa kamu prospek untuk menaikkan penjualan.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/9-impor-data-pelanggan.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Mengimpor data invoice dan penawaran dalam jumlah besar</h3>
										<p class="fs-6 opacity-5 text-break">Tak perlu lagi khawatir memasukkan ratusan data dalam waktu singkat. Cukup upload file Excel-mu, dan kledo akan mengolahnya menjadi invoice.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card-stack-item mb-4">
					<div class="card p-2 p-md-3 rounded-2 border-0 shadow">
						<div class="card-body">
							<div class="row">
								<div class="col-md-7 order-md-2">
									<img class="img-fluid" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/invoice/10-stock-gudang-tercatat.webp" alt="Screenshot">
								</div>
								<div class="col-md-5 order-md-1 d-flex align-items-center">
									<div class="text-center text-md-left mb-2 mt-3 mt-md-2">
										<h3 class="fs-3 text-break mb-3">Stok dan gudang tercatat otomatis</h3>
										<p class="fs-6 opacity-5 text-break">Stok produk akan tercatat secara otomatis di gudang yang kamu tentukan. Tak perlu lagi double input untuk urusan inventori!</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<style>
				@media (min-width: 768px) {
					.card-stack {
						display: grid;
						grid-auto-rows: 1fr;
					}
					.card-stack-item {
						position: -webkit-sticky;
						position: sticky;
						top: 200px;
					}
					.card-stack-item > .card {
						height: 100%;
						-webkit-transform-origin: top center;
						-ms-transform-origin: top center;
						transform-origin: top center;
					}
					.card-stack-item > .card > .row {
						height: inherit;
					}
				}
			</style>
			<script>
				let e = document.querySelectorAll('.card-stack'),
					o = ['load', 'resize', 'scroll'];
				[].forEach.call(e, function(e) {
					let t = e.querySelectorAll('.card-stack-item');
					o.forEach(function(e) {
						window.addEventListener(e, function() {
							let d = [].slice.call(t).reverse();
							d.reduce(function(e, t, o) {
								let n = t.clientHeight + parseInt(window.getComputedStyle(t).getPropertyValue('margin-bottom')),
								a = e + (n - (d[o - 1] ? d[o - 1].offsetTop - t.offsetTop : n)) / n,
								r = t.firstElementChild,
								l = r.firstElementChild,
								c = 'calc(-1rem * ' + a + ')',
								i = 'calc(1 - .2 * ' + a + ')',
								s = 'calc(1 - .03 * ' + a + ')';
								return r.style.transform = 'translateY(' + c + ') scale(' + s + ')', l.style.opacity = i, a
							}, 0)
						})
					})
				})
			</script>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>