<?php /* Template Name: Tutorial Template */ ?>

<?php get_header() ?>

    <article id="header" class="bg-theme-1 text-white pb-md-4 pb-lg-5 pt-lg-3">
		<div class="container pt-5 pb-5 pb-md-3">
			<h1 class="fs-1 text-break text-center mb-0">
				Semua Tutorial & Cara Penggunaan
			</h1>
			<p class="fs-6 opacity-7 text-break text-center mt-2 mb-0">
				Cari yang ingin kamu pelajari dengan submit form di bawah. <br class="d-none d-md-inline-block">
				Jika tidak menemukan yang kamu cari, silahkan hubungi tim Hebat Kledo yah :)
			</p>
			<div class="row">
				<div class="col-md-9 col-lg-8 col-xl-7 mx-auto">
					<div class="bg-white p-3 rounded-pill shadow mt-4" id="findTutorial">
						<form class="form-inline" spellcheck="false" autocomplete="off">
							<div class="d-flex w-100">
								<label class="sr-only" for="input-keyword">Kata Kunci</label>
								<input type="text" name="keyword" class="form-control bg-transparent border-0 w-100 pl-3" id="input-keyword" placeholder="Apa yang ingin Anda cari?">
								<div class="d-flex">
									<button type="reset" class="btn btn-clear text-danger p-0 mr-1" style="display:none">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="25" height="25" fill="var(--theme-1)">
											<path d="M17.7,18.8L5.2,6.3C5,6,5,5.5,5.2,5.2C5.5,5,6,5,6.3,5.2l12.4,12.4c0.3,0.3,0.3,0.8,0,1.1C18.5,19,18,19,17.7,18.8z"/>
											<path d="M5.2,17.7L17.7,5.2C18,5,18.5,5,18.8,5.2C19,5.5,19,6,18.8,6.3L6.3,18.8C6,19,5.5,19,5.2,18.8C5,18.5,5,18,5.2,17.7z"/>
										</svg>
										<span class="sr-only">Reset</span>
									</button>
									<button type="submit" class="btn btn-clear py-0 opacity-5">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="var(--black)">
											<path d="M11.5 21.75C5.85 21.75 1.25 17.15 1.25 11.5C1.25 5.85 5.85 1.25 11.5 1.25C17.15 1.25 21.75 5.85 21.75 11.5C21.75 17.15 17.15 21.75 11.5 21.75ZM11.5 2.75C6.67 2.75 2.75 6.68 2.75 11.5C2.75 16.32 6.67 20.25 11.5 20.25C16.33 20.25 20.25 16.32 20.25 11.5C20.25 6.68 16.33 2.75 11.5 2.75Z"/>
											<path d="M22.0004 22.7499C21.8104 22.7499 21.6204 22.6799 21.4704 22.5299L19.4704 20.5299C19.1804 20.2399 19.1804 19.7599 19.4704 19.4699C19.7604 19.1799 20.2404 19.1799 20.5304 19.4699L22.5304 21.4699C22.8204 21.7599 22.8204 22.2399 22.5304 22.5299C22.3804 22.6799 22.1904 22.7499 22.0004 22.7499Z"/>
										</svg>
										<span class="sr-only">Search</span>
									</button>
								</div>
							</div>
						</form>
					</div>
					<div class="position-absolute text-center mt-3" id="resultTutorial" style="width:calc(100% - 2rem);display:none">
						Ditemukan <span></span> hasil pencarian
					</div>
				</div>
			</div>
		</div>
	</article>

	<article>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1)" class="overlay-top overlay-flip-y">
			<polygon points="100,30 0,30 0,25 100,0"/>
		</svg>
		<div class="container py-5">
			<div class="row">
				<div class="col-md-4 col-lg-3">
					<form spellcheck="false" class="pills-responsive pt-2 pb-2 pb-md-3 mt-n2 mb-3 mb-md-4 position-sticky">
						<div class="mb-2">
							<span class="small opacity-6 text-uppercase">Kategori</span>
						</div>
						<div class="form-group d-md-none">
							<select class="form-control custom-select">
								<option value="0">Mulai Menggunakan Kledo</option>
								<option value="1">Pembelian</option>
								<option value="2">Penjualan</option>
								<option value="3">Produk</option>
								<option value="4">Produk</option>
								<option value="5">Biaya</option>
								<option value="6">Aset Tetap</option>
								<option value="7">Pengaturan</option>
								<option value="8">Laporan</option>
								<option value="9">Inventory / Persediaan</option>
								<option value="10">Akun / COA</option>
								<option value="11">Kontak</option>
							</select>
						</div>
						<div class="d-none d-md-block">
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-0">
								Mulai Menggunakan Kledo
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-1">
								Pembelian
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-2">
								Penjualan
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-3">
								Produk
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-4">
								Kas
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-5">
								Biaya
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-6">
								Aset Tetap
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-7">
								Pengaturan
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-8">
								Laporan
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-9">
								Inventory / Persediaan
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-10">
								Akun / COA
							</a>
							<a class="btn btn-clear btn-block rounded-2 px-3 text-left" href="#category-11">
								Kontak
							</a>
						</div>
					</form>
				</div>
				<div class="col-md-8 col-lg-9">
					<div class="row grid">
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-0">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Mulai Menggunakan Kledo
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Mulai Menggunakan Kledo</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Data Demo</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Profil dan Ganti Password</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Daftar & Login</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-1">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Pembelian
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Cicilan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Kontak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Pembayaran Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Pesanan Pembelian (Purchase Order)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Termin Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Status Transaksi Pesanan Pembelian (Purchase Order)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Status Transaksi Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Memperoleh Overview Pembelian</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Tagihan Pembelian (Purchase Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Pesanan Pembelian (Purchase Order)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Pesanan Pembelian (Purchase Order)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Pesanan Pembelian (Purchase Order)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Fungsi Overview Pembelian</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-2">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Penjualan
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Tutorial Print Tagihan Massal</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menggunakan Scan Barcode</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Print Faktur Pajak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Kontak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Penawaran Penjualan (Quote)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Penawaran Penjualan (Quote)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Pembayaran Tagihan Penjualan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Tagihan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Memperoleh Overview Penjualan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Tagihan Penjualan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Tagihan Penjualan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Status Transaksi Tagihan Penjualan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Status Transaksi Penawaran Penjualan (Quote)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Tagihan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Penawaran (Quote)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Penawaran (Quote)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Termin Tagihan Penjualan (Invoice)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Fungsi Overview Penjualan</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-3">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Produk
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Penjualan dengan Produk Paket</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Proses Produksi dan Menjual Produk Manufaktur</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Kategori Produk</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Produk</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Produk</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menambah Produk</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Produk</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-4">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Kas
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Prive</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Transfer Dana Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Rekonsiliasi Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Kirim Dana Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Terima Dana Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menambah Akun Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Melihat Transaksi Akun Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Akun Kas & Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Bank Statement Manual</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-5">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Biaya
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Piutang Karwayan (Kasbon)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengelompokkan Transaksi</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Kontak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Status Transaksi Biaya (Expense)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Pembayaran Biaya (Expense)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Biaya (Expense)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Biaya (Expense)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Termin Biaya (Expense)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Posting Biaya (Expense)</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-6">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Aset Tetap
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menerapkan Penyusutan Otomatis</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengembalikan Penyusutan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menambah Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menjual/Melepas Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Melihat Detail Aset Tetap</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-7">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Pengaturan
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Audit Log</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Tags</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Layout Invoice</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Upgrade Paket Pro</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Data Demo</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Termin</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Pajak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Penomoran Otomatis</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Tanggal Penguncian</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Pengguna</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Peran</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Bahasa</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Perusahaan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Profil dan Ganti Password</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-8">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Laporan
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Konsolidasi</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Sekilas tentang Beranda</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Ringkasan Inventori</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pergerakan Stok Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Ringkasan Stok Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pergerakan Stok Inventori</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Biaya per Kontak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Ringkasan Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Detil Aset Tetap</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Umur Piutang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Penjualan per Produk</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pembelian per Produk</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Detil Klaim Biaya</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pendapatan per Pelanggan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pembelian per Vendor</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Jurnal</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Trial Balance</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Umur Hutang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Tagihan Pelanggan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Tagihan Vendor</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pajak Pemotongan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Ringkasan Bank</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Buku Besar</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Neraca</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Laba Rugi</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Ringkasan Eksekutif</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Pajak Penjualan</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Laporan Arus Kas</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-9">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Inventory / Persediaan
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Konsinyasi (Titip Jual)</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Alur Pencatatan Inventori</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengatur Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Transfer Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Transfer Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Penyesuaian Stok</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Penyesuaian Stok</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menambah Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Transfer Masuk Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Transfer Keluar Gudang</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Penyesuaian Stok</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-10">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Akun / COA
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Import Jurnal Umum</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencatat Modal Usaha</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menentukan Saldo Normal Akun</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Akun</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Jurnal Umum</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menghapus Jurnal Umum</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Menambah Akun</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengisi Saldo Awal</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Akun</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mencetak Detail Akun</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3 grid-item">
							<div class="card p-2 p-md-3 rounded-2 border-0 shadow" id="category-11">
								<div class="card-body py-0">
									<h2 class="fs-5 text-theme-1 my-2 pt-1">
										Kontak
									</h2>
									<ul class="list-group list-group-flush list-tutorial">
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Membuat Kontak</a>
										</li>
										<li class="list-group-item bg-transparent p-0">
											<a class="text-decoration-none color-inherit d-inline-block py-3" href="#">Cara Mengubah Kontak</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<style>
			.list-tutorial {
				position: relative;
			}
			.list-tutorial:before {
				content: '';
				position: absolute;
				width: 100%;
				height: 3px;
				bottom: -1px;
				background: var(--white);
				z-index: 1;
			}
		</style>
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				function findItem(form,count,list) {
					$(list).each(function() {
						$('<div class="opacity-3 my-3" role="alert" style="display:none"><em>Tidak ditemukan di kategori ini</em></div>').appendTo($(this).closest('.card-body'))
					})

					$(form + ' input').on('keyup', function() {
						let value = $(this).val()
							keywords = new RegExp(value,'i')

						$(list + ' > li').each(function() {
							let item = $(this)
								keyword = item.find('a').text()
							if (!keyword.search(keywords) >= 0) {
								item.hide()
							}
							if (keyword.search(keywords) >= 0) {
								item.show()
							}
						})

						$(list).each(function() {
							let item = $(this)
							if ( item.find('li:visible').length < 1 ) {
								item.next().show()
							} else {
								item.next().hide()
							}
						})

						if ( value.length > 0 ) {
							$(count).show()
							$(form + ' form [type="reset"]').show()
						} else {
							$(count).hide()
							$(form + ' form [type="reset"]').hide()
						}

						$(count + ' span').html($(list + ' li:visible').length)

						$('.grid').masonry()
					})

					$(form + ' form').on('reset',function(e) {
						$(e.target).find('[type="reset"]').hide()
						$(list + ' > li').show()
						$(list + ' + [role="alert"]').hide()
						$(count).hide()
						$('.grid').masonry()
					})

					$(form + ' form').on('submit',function(e) {
						e.preventDefault()
					})
				}
				findItem('#findTutorial','#resultTutorial','.list-tutorial')

				function orderItem() {
					let masonry = document.createElement('script')
						masonry.id = 'masonry'
						masonry.src = 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.2.2/masonry.pkgd.min.js'
					document.querySelector('slot').appendChild(masonry)

					let checkMasonry = setInterval(() => {
						if ( $('#masonry').length > 0 ) {
							$('.grid').masonry({
								itemSelector: '.grid-item'
							})
							clearInterval(checkMasonry)
						}
					},500)
				}
				orderItem()

				function gotoItem(parent) {
					$(parent + ' .btn-clear:first-child').addClass('active')

					$(parent + ' .custom-select').on('change', function() {
						let selectVal = this.value
						$(parent + ' .btn-clear').each(function() {
							let link = $(this)
								linkVal = link.attr('href')
								linkVal = linkVal.split('-')
							link.removeClass('active')
							if ( selectVal == linkVal[1] ) {
								link.addClass('active')
								link.click()
							}
						})
					})

					$(parent + ' .btn-clear').click(function(e) {
						let link = $(this)
							linkVal = link.attr('href')
							linkVal = linkVal.split('-')
						$(parent + ' .custom-select').val(linkVal[1])
						$(parent + ' .btn-clear').removeClass('active')
						link.addClass('active')
						$('html, body').animate({
							scrollTop: $($.attr(this, 'href')).offset().top - 100
						},500)
					})
				}
				gotoItem('.pills-responsive')
			})
		</script>
	</article>

	<article>
		<div class="container py-5 position-relative text-center">
			<div class="row">
				<div class="col-12">
					<h2 class="fs-2 text-break mb-3">
						Ada pertanyaan?
					</h2>
					<p class="fs-6 opacity-6 mb-3 pb-1">
						Silakan hubungi kami melalui WhatsApp
					</p>
					<a class="btn btn-success rounded-pill py-2 pl-3 pr-4 lh-1 fs-6 font-title text-uppercase ws-nowrap" href="https://wa.me/6282383334000&text=Hallo, Saya mau tanya tentang Kledo.">
						<div class="d-flex align-items-center">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="currentColor" class="m-1">
								<path d="M12.04 2C6.58 2 2.13 6.45 2.13 11.91C2.13 13.66 2.59 15.36 3.45 16.86L2.05 22L7.3 20.62C8.75 21.41 10.38 21.83 12.04 21.83C17.5 21.83 21.95 17.38 21.95 11.92C21.95 9.27 20.92 6.78 19.05 4.91C17.18 3.03 14.69 2 12.04 2M12.05 3.67C14.25 3.67 16.31 4.53 17.87 6.09C19.42 7.65 20.28 9.72 20.28 11.92C20.28 16.46 16.58 20.15 12.04 20.15C10.56 20.15 9.11 19.76 7.85 19L7.55 18.83L4.43 19.65L5.26 16.61L5.06 16.29C4.24 15 3.8 13.47 3.8 11.91C3.81 7.37 7.5 3.67 12.05 3.67M8.53 7.33C8.37 7.33 8.1 7.39 7.87 7.64C7.65 7.89 7 8.5 7 9.71C7 10.93 7.89 12.1 8 12.27C8.14 12.44 9.76 14.94 12.25 16C12.84 16.27 13.3 16.42 13.66 16.53C14.25 16.72 14.79 16.69 15.22 16.63C15.7 16.56 16.68 16.03 16.89 15.45C17.1 14.87 17.1 14.38 17.04 14.27C16.97 14.17 16.81 14.11 16.56 14C16.31 13.86 15.09 13.26 14.87 13.18C14.64 13.1 14.5 13.06 14.31 13.3C14.15 13.55 13.67 14.11 13.53 14.27C13.38 14.44 13.24 14.46 13 14.34C12.74 14.21 11.94 13.95 11 13.11C10.26 12.45 9.77 11.64 9.62 11.39C9.5 11.15 9.61 11 9.73 10.89C9.84 10.78 10 10.6 10.1 10.45C10.23 10.31 10.27 10.2 10.35 10.04C10.43 9.87 10.39 9.73 10.33 9.61C10.27 9.5 9.77 8.26 9.56 7.77C9.36 7.29 9.16 7.35 9 7.34C8.86 7.34 8.7 7.33 8.53 7.33Z"/>
							</svg>
							<span class="mx-1">
								0823 8333 4000
							</span>
						</div>
					</a>
				</div>
			</div>
			<img class="obj-fit-cover obj-position-top position-absolute right bottom mb-n5 mr-5 d-none d-md-block" loading="lazy" width="275" height="300" src="upload/woman.png">
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>