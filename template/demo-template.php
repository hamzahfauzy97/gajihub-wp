<?php /* Template Name: Demo Template */ ?>

<?php get_header() ?>

	<script>
	document.addEventListener('DOMContentLoaded', function() {
		navbarScroll('.navbar','.shadow','10')
	})
	</script>
	<article id="header">
		<script>
			document.querySelectorAll('.navbar-dark').forEach(nav => {
				nav.classList.add('navbar-light')
				nav.classList.remove('navbar-dark')
			})
		</script>
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						Demo & Konsultasi Gratis
					</h1>
					<p class="fs-6 opacity-7 text-break text-center mt-2 mb-0 mx-md-5">
						Diskusikan permasalahan perusahaan Anda, untuk mendapatkan solusi terbaik menggunakan Kledo. Demo dan konsultasi gratis dilakukan online via Zoom 1 on 1 selama 45 menit bersama tim hebat Kledo.
					</p>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="container py-4">
			<p class="fs-6 opacity-7 text-break text-center mt-2 mb-0 mx-md-5">
				Silakan pilih jadwal untuk demo dan konsultasi online berikut:
			</p>
		</div>
	</article>

	<article>
		<div class="mb-4">
			<div class="calendly-inline-widget" data-url="https://calendly.com/kledo/online-demo"></div>
			<script src="https://assets.calendly.com/assets/external/widget.js" async></script>
			<style>
				.calendly-inline-widget {
					min-width: 320px;
					height: calc(100vh - 100px);
				}
				@media (min-width: 768px) {
					.calendly-inline-widget {
						height: 650px;
					}
				}
			</style>
		</div>
	</article>

	<article>
		<div class="container py-4">
			<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
				<div class="text-break fs-6">
					<p class="fs-5 font-weight-bold mb-2">Catatan:</p>
					<ul class="pl-4 mb-4 pb-3">
						<li>Masukan data diri dengan benar untuk kemudahan komunikasi dan konfirmasi antara Anda dan tim Kledo nanti.</li>
						<li>Setelah Anda melakukan scheduling sesi demo dan konsultasi gratis dengan tim Kledo, kami akan akan segera mengonfirmasi melalui whatsapp atau email.</li>
						<li>Pastikan Anda memilih waktu yang tepat agar proses demo berjalan lancar dan Anda bisa mendapatkan semua informasi yang dibutuhkan.</li>
						<li>Anda bisa menggunakan smartphone, laptop, atau PC saat melakukan sesi demo atau konsultansi nanti. Pastikan juga Anda memiliki koneksi internet yang stabil.</li>
					</ul>
					<p>Jika Anda ingin mengenal Kledo secara singkat sebelum sesi demo berlangsung, Anda bisa <a class="text-decoration-none" href="proposal.html">mengunduh proposal ringkas</a> dari Kledo.</p>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="container py-5 position-relative text-center">
			<div class="row">
				<div class="col-12">
					<h2 class="fs-2 text-break mb-3">
						Ada pertanyaan?
					</h2>
					<p class="fs-6 opacity-6 mb-3 pb-1">
						Silakan hubungi kami melalui WhatsApp
					</p>
					<a class="btn btn-success rounded-pill py-2 pl-3 pr-4 lh-1 fs-6 font-title text-uppercase ws-nowrap" href="https://wa.me/6282383334000&text=Hallo, Saya mau tanya tentang Kledo.">
						<div class="d-flex align-items-center">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="currentColor" class="m-1">
								<path d="M12.04 2C6.58 2 2.13 6.45 2.13 11.91C2.13 13.66 2.59 15.36 3.45 16.86L2.05 22L7.3 20.62C8.75 21.41 10.38 21.83 12.04 21.83C17.5 21.83 21.95 17.38 21.95 11.92C21.95 9.27 20.92 6.78 19.05 4.91C17.18 3.03 14.69 2 12.04 2M12.05 3.67C14.25 3.67 16.31 4.53 17.87 6.09C19.42 7.65 20.28 9.72 20.28 11.92C20.28 16.46 16.58 20.15 12.04 20.15C10.56 20.15 9.11 19.76 7.85 19L7.55 18.83L4.43 19.65L5.26 16.61L5.06 16.29C4.24 15 3.8 13.47 3.8 11.91C3.81 7.37 7.5 3.67 12.05 3.67M8.53 7.33C8.37 7.33 8.1 7.39 7.87 7.64C7.65 7.89 7 8.5 7 9.71C7 10.93 7.89 12.1 8 12.27C8.14 12.44 9.76 14.94 12.25 16C12.84 16.27 13.3 16.42 13.66 16.53C14.25 16.72 14.79 16.69 15.22 16.63C15.7 16.56 16.68 16.03 16.89 15.45C17.1 14.87 17.1 14.38 17.04 14.27C16.97 14.17 16.81 14.11 16.56 14C16.31 13.86 15.09 13.26 14.87 13.18C14.64 13.1 14.5 13.06 14.31 13.3C14.15 13.55 13.67 14.11 13.53 14.27C13.38 14.44 13.24 14.46 13 14.34C12.74 14.21 11.94 13.95 11 13.11C10.26 12.45 9.77 11.64 9.62 11.39C9.5 11.15 9.61 11 9.73 10.89C9.84 10.78 10 10.6 10.1 10.45C10.23 10.31 10.27 10.2 10.35 10.04C10.43 9.87 10.39 9.73 10.33 9.61C10.27 9.5 9.77 8.26 9.56 7.77C9.36 7.29 9.16 7.35 9 7.34C8.86 7.34 8.7 7.33 8.53 7.33Z"/>
							</svg>
							<span class="mx-1">
								0823 8333 4000
							</span>
						</div>
					</a>
				</div>
			</div>
			<img class="obj-fit-cover obj-position-top position-absolute right bottom mb-n5 mr-5 d-none d-md-block" loading="lazy" width="275" height="300" src="upload/woman.png">
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>