<?php /* Template Name: About Template */ ?>

<?php get_header() ?>

<article id="header" class="pb-md-4 pb-lg-5 pt-lg-3">
		<script>
			document.querySelectorAll('.navbar-dark').forEach(nav => {
				nav.classList.add('navbar-light')
				nav.classList.remove('navbar-dark')
			})
		</script>
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						Tentang Kledo
					</h1>
				</div>
			</div>
		</div>
	</article>

	<article id="section-1">
		<section class="parallax text-white" data-parallax="https://kledo.com/wp-content/uploads/2020/05/kledo-building.jpg">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-top overlay-flip-y">
				<polygon points="100,30 0,30 0,25 100,0"/>
			</svg>
			<div class="container py-5">
				<div class="row py-3">
					<div class="col-md-6">
						<img class="img-fluid rounded-2 shadow mb-4" width="600" height="400" src="https://kledo.com/wp-content/uploads/2020/05/kledo-building-768x605.jpg">
					</div>
					<div class="col-md-6">
						<p class="fs-6 lh-4">Kledo dimulai untuk mengubah bisnis kecil. Dari yang serba konvensional, menjadi bisnis yang bankable, auditable dan mampu bersaing di kancah nasional dan global. Software akuntansi online Kledo membantu merapikan sisi keuangan dan proses bisnis usaha kecil dan menengah. Kledo telah membantu lebih dari 5.000 pebisnis, dan kami baru memulainya. Kami tak akan berhenti berjuang bersama usaha kecil Indonesia. Saat ini bisnis kecil identik dengan pencatatan yang tidak rapi, pencatatan pada tumpukan kertas, dan tidak tertib administrasi. Hal itu salah satunya disebabkan karena minimnya akses pebisnis UMKM ke teknologi terbaru yang sangat memudahkan. Karena itu Kledo hadir, mendekatkan pebisnis pada teknologi, dan meng-upgrade bisnis ke level berikutnya.</p>
					</div>
				</div>
			</div>
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-bottom">
				<polygon points="0,0 100,25 100,30 0,30"/>
			</svg>
		</section>
		<style>
			#section-1 .parallax:after {
				background-color: rgba(229,62,106,.85);
			}
		</style>
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				function scrollParallax(id,overscroll) {
					document.querySelectorAll(id).forEach(selector => {
						let element = selector.id
							slot = document.querySelector('slot')

						let desktop = document.createElement('style')
							desktop.id = element + '-css'
						slot.appendChild(desktop)

						let mobile = document.createElement('style')
							mobile.id = element + '-css-mobile'
							mobile.innerText = '@media (max-width:991px){#' + element + ' .parallax:before{background-size:cover!important;background-position-y:initial!important}}'
						slot.appendChild(mobile)

						function startEffect() {
							let scroll = window.pageYOffset || document.documentElement.scrollTop
								item = document.querySelector('#' + element).clientHeight
							if ( scroll <= (item + overscroll) ) {
								document.querySelector('#' + element + '-css').innerText = '#' + element + ' .parallax:before{background-size:calc(100vmax + ' + scroll + 'px);background-position-y: -' + (scroll + (scroll / 2)) + 'px}'
							}
						}

						if ( document.body.clientWidth >= 992 ) {
							startEffect()
							window.onscroll = () => {
								startEffect()
							}
						}
					})
				}
				scrollParallax('#section-1',200)
			})
		</script>
	</article>

	<article>
		<div class="container py-5">
			<div class="row py-3">
				<div class="col-md-6">
					<p class="fs-6 lh-4">Berkantor pusat di Jogja, Kledo dikelilingi oleh talenta-talenta muda yang sangat inovatif. Kota pelajar yang dipenuhi oleh anak muda dengan kemampuan teknologi tinggi dan mempunyai gairah besar untuk membantu negeri.</p>
					<br>
					<div class="fs-6 mb-2">
						<b>Hubungi Kami :</b>
					</div>
					<div class="fs-6 mb-2">
						<a class="text-decoration-none color-inherit" href="https://wa.me/6282383334000" rel="noopener" target="_blank">
							+62 823 8333 4000 (WA only)
						</a>
					</div>
					<div class="fs-6 mb-2">
						<a class="text-decoration-none color-inherit" href="mailto:hello@gajihub.com" rel="noopener" target="_blank">
							hello@gajihub.com
						</a>
					</div>
					<div class="fs-6 mb-2">
						<a class="text-decoration-none color-inherit" href="https://goo.gl/maps/nhV6K5kEeW5BNAUW6" rel="noopener" target="_blank">
							Jl. Kledokan No B10, Catur Tunggal, Depok, DIY, 55281
						</a>
					</div>
				</div>
				<div class="col-md-6">
					<div class="embed-responsive embed-responsive-16by9 position-relative bg-light rounded-2 shadow mt-4 mt-md-0 gmaps">
						<div class="position-absolute top left w-100 h-100 d-flex align-items-center justify-content-center loading">
							<div class="spinner-border text-danger" role="status">
								<span class="sr-only">Loading...</span>
							</div>
						</div>
					</div>
					<script>
						const gmapsEmbed = `https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7906.2677509341775!2d110.4077484!3d-7.7756261!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a59becaae75e9%3A0xf9c76923bed0af5b!2sOase%20Organik!5e0!3m2!1sen!2sid!4v1639619516310!5m2!1sen!2sid`
						
						document.addEventListener('DOMContentLoaded', function() {
							let location = document.querySelector('.gmaps')
								gmapsLoad = location.querySelector('.loading')
								gmaps = document.createElement('iframe')
								gmaps.title = 'Google Maps'
								gmaps.src = gmapsEmbed
								gmaps.width = 600
								gmaps.height = 450
								gmaps.style = 'border:0;'
								gmaps.className = 'embed-responsive-item'
								gmaps.setAttribute('allowfullscreen','')
								gmaps.setAttribute('loading','lazy')
							setTimeout(function() {
								location.appendChild(gmaps)
								gmapsLoad.parentNode.removeChild(gmapsLoad)
							},2500)
						})
					</script>
				</div>
			</div>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>