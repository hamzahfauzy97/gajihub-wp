<?php /* Template Name: Term Template */ ?>

<?php get_header() ?>
	
	<article id="header" class="bg-theme-1 text-white pb-md-4 pb-lg-5 pt-lg-3">
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						Syarat & Ketentuan Kledo
					</h1>
				</div>
			</div>
		</div>
	</article>

	<article>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1)" class="overlay-top overlay-flip-y">
			<polygon points="100,30 0,30 0,25 100,0"/>
		</svg>
		<div class="container py-5">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto post-content">
					<h2 class="fs-4 lh-3 mb-4">Syarat-syarat ini mengikat dan mulai berlaku untuk Anda saat Anda mulai mengakses layanan apapun dari Kledo.</h2>
					<p class="ml-0">Layanan Kledo dapat berubah dari waktu ke waktu berdasarkan kebijakan Kledo, keputusan pemerintah, kesan & saran dari pengguna dan perubahan teknis dari sistem. Syarat-syarat ini tidak dimaksudkan untuk menjawab setiap pertanyaan atau menunjukkan setiap masalah yang dapat timbul dari penggunaan layanan Kledo. Kledo berhak untuk mengubah Syarat & Ketentuan ini kapan saja, dan mulai berlaku pada saat Syarat & Ketentuan yang baru atau yang telah direvisi di lampirkan di Website Kledo. Kledo akan berusaha untuk mengumumkan perubahan-perubahannya kepada Anda melalui email atau pemberitahuan melalui Website. Karena cukup memungkinan bahwa Syarat & Ketentuan ini akan berubah dari waktu ke waktu, maka menjadi kewajiban Anda untuk memastikan bahwa Anda sudah membaca, mengerti, dan menyetujui Syarat & Ketentuan terbaru yang tersedia pada Website Kledo. Dengan mendaftarkan diri untuk menggunakan layanan Kledo, Anda menyatakan bahwa Anda sudah membaca, mengerti dan menyetujui Syarat & Ketentuan ini, dan dianggap memiliki wewenang untuk bertindak atas nama siapapun yang terdaftar untuk menggunakan layanan kami.</p>
					<br>
					<h3 id="definisi">1. Syarat & Ketentuan Kledo</h3>
					<ol>
						<li><strong>Perjanjian</strong> – berarti Syarat & Ketentuan ini;</li>
						<li><strong>Biaya Akses</strong> – berarti biaya bulanan (belum termasuk pajak) yang harus Anda bayar sesuai daftar biaya yang dicantumkan pada Website Kledo (yang Kledo dapat merubah dari waktu ke waktu dengan sepengetahuan Anda);</li>
						<li><strong>Informasi Rahasia</strong> – melingkup semua informasi yang dikomunikasikan antara pihak Perjanjian ini, baik secara tertulis, elektronik, atau lisan, termasuk Layanan ini, tapi tidak termasuk informasi yang sudah menjadi atau akan dijadikan untuk public, terkecuali yang sudah diungkapkan dan terbongkar tanpa hak atau oleh pihak pengguna atau lainnya secara tidak sah;</li>
						<li><strong>Data</strong> – berarti data apapun yang Anda masukan atau dimasukan dengan kewenangan Anda kepada Website;</li>
						<li><strong>Hak Kekayaan Intelektual</strong> – berarti paten, merek dagang, merek jasa atau layanan, hak cipta, hak pada disain, pengetahuan, atau hak kekayaan intelektual atau industri lainnya, maupun terdaftar atau tidak terdaftar;</li>
						<li><strong>Layanan</strong> – berarti layanan pengelolaan akunting, finansial, dan operasional yang disediakan (dan dapat dirubah atau diperbarui dari waktu ke waktu) melalui Website maupun aplikasi mobile;</li>
						<li><strong>Website</strong> – berarti situs internet di domain <a href="https://kledo.com">kledo.com</a> atau situs internet lainnya yang di kelola oleh Kledo;</li>
						<li><strong>Kledo</strong> – berarti PT Kledo Berhati Nyaman yang terdaftar di Indonesia;</li>
						<li><strong>Pengguna Diundang</strong> – berarti setiap orang atau badan, selain Pelanggan, yang memakai Layanan dari waktu ke waktu dengan izin dari Pelanggan;</li>
						<li><strong>Pelanggan</strong> – berarti orang, maupun atas nama pribadi, organisasi atau badan lainnya, yang mendaftar untuk menggunakan Layanan;</li>
						<li><strong>Anda</strong> – berarti Pelanggan, Pengguna atau Pengguna Diundang.</li>
					</ol>
					<br>
					<h3 id="penggunaan-software">2. Penggunaan <em>Software</em></h3>
					<p>Kledo memberi Anda hak untuk mengakses dan memakai Layanan Kledo melalui Website kami dengan peran penggunaan yang sudah ditentukan untuk Anda, sesuai dengan jenis layanan yang Anda pilih. Hak ini non-eksklusif, tidak dapat dipindahtangankan, dan dibatasi oleh dan bergantung pada perjanjian ini. Anda mengakui dan menyutujui, bergantung kepada perjanjian tulis apapun yang berlaku antara Pelanggan dan Pengguna Diundang, atau hukum lainnya yang berlaku:</p>
					<ol>
						<li>bahwa adalah tanggung jawab Pelanggan untuk menentukan siapa yang mendapat akses sebagai Pengguna Diundang dan jenis peran dan hak yang mereka punya untuk mengakses jenis data yang Anda miliki;</li>
						<li>bahwa tanggung jawab Pelanggan untuk semua penggunaan Layanan oleh Pengguna Diundang;</li>
						<li>bahwa tanggung jawab Pelanggan untuk mengendalikan setiap tingkat akses untuk Pengguna Diundang kepada organisasi dan Layanan yang relevan setiap saat, dan bisa menarik atau mengubah akses atau tingkat akses Pengguna Diundang kapanpun, untuk alasan apapun di dalam kasus manapun;</li>
						<li>jika ada perselisihan antara Pelanggan dan Pengguna Diundang mengenai akses kepada organisasi atau Layanan apapun, Pelanggan lah yang harus membuat keputusan dan mengatur akses atau tingkat akses ke Data atau Layanan yang Pengguna Diundang akan dapat, jika ada.</li>
					</ol>
					<br>
					<h3 id="ketersediaan-layanan">3. Ketersediaan Pelayanan Kami</h3>
					<ol>
						<li><strong>Layanan</strong>: Layanan kami memiliki jaminan 90% uptime minimal perbulan. Jika layanan kami jatuh di bawah 90% uptime, maka pelanggan kami berhak mengajukan laporan dan mendapatkan kompensasi, dengan besar maksimal kompensasi adalah sama dengan biaya berlangganan paket Kledo dari akun Anda selama 1 tahun.</li>
						<li><strong>Product Support: </strong>Pelayanan Product Support kami tersedia melalui 3 media:
							<ol>
								<li>Chat: 9.00 – 18.00, Hari Senin – JumatChat yang diterima di luar jam kerja akan dibalas melalui email dalam kurun waktu 24 jam.</li>
								<li>Telepon: 9.00 – 18.00, Hari Senin – Jumat</li>
								<li>Email: 9.00 – 18.00, Hari Senin – Jumat Email yang diterima di luar jam kerja akan dibalas dalam kurun waktu 24 jam.</li>
							</ol>
						</li>
					</ol>
					<br>
					<h3 id="kewajiban-anda">4. Kewajiban Anda</h3>
					<ol>
						<li><b>Kewajiban Pembayaran: </b>Tagihan untuk Biaya Akses akan di buat setiap bulan, mulai satu bulan dari tanggal Anda mulai berlangganan untuk Layanan Kledo. Semua tagihan dikenakan atas Biaya Akses untuk periode Masa Aktif pemakaian Layanan. Kledo akan terus membuat tagihan untuk Anda setiap akhir periode Masa Aktif sampai perjanjian ini diakhiri sesuai dengan ketentuan pada Pasal 9.
						Masa Aktif otomatis akan dimulai paling lambat 30 hari setelah Anda melakukan pembayaran Biaya Akses untuk pertama kalinya. Semua tagihan dari Kledo akan dikirim kepada Anda, atau kepada kontak penagihan yang telah Anda berikan melalui email. Anda harus membuat pembayaran sesuai nilai yang tertera di tagihan Anda sebelum melewati tanggal jatuh tempo untuk tagihannya dan harus dilunaskan 7 hari dari saat tagihan dikirim oleh Kledo. Anda mempunyai tanggung jawab untuk pembayaran semua pajak dan bea yang ditambahkan kepada Biaya Akses tersebut (jika ada) dan harus menyimpan bukti transaksi. Jika layanan sempat terhenti sebelum pembayaran dilakukan, kami akan mengaktifkan layanan setelah Anda melakukan pembayaran Biaya Akses untuk perpanjangan Masa Aktif.</li>
						<li><b>Kewajiban Umum: </b>Anda harus memastikan hanya memakai Layanan dan Website untuk keperluan internal bisnis Anda yang benar dan secara sah, dengan Syarat dan Ketentuan dan pemberitahuan yang diumumkan oleh Kledo atau ketentuan yang tercantum di Website.
						Anda boleh memakai Layanan dan Website atas nama orang atau badan lain, atau untuk memberi layanan kepada mereka, tetapi Anda harus memastikan bahwa Anda mempunyai wewenang untuk melakukannya, dan semua pihak yang menerima Layanan melalui Anda memenuhi dan menyetujui semua syarat dalam Perjanjian ini yang berlaku kepada Anda;</li>
						<li><b>Syarat Akses: </b>Anda harus memastikan bahwa semua username dan password yang diperlukan untuk mengakses Layanan Kledo tersimpan dengan aman dan secara rahasia. Anda harus segera memberitahu Kledo mengenai pemakaian password Anda dari pihak yang tidak berwenang, atau pelanggaran keamanan lainnya, dan Kledo akan reset password Anda, dan Anda harus melakukan semua tindakan lainnya yang dianggap cukup penting oleh Kledo untuk mempertahankan atau meningkatkan keamanan sistem computer dan jaringan Kledo, dan akses Anda kepada Layanan kami.Sebagai syarat dari Ketentuan-ketentuan ini, saat mengakses dan menggunakan Layanan Kledo, Anda harus:
							<ol>
								<li>Tidak mencoba untuk melemahkan keamanan atau integritas dari sistem komputer atau jaringan Kledo, atau jika Layanan-nya di <em>host</em> oleh pihak ketiga, sistem komputer atau jaringan pihak ketiga itu;</li>
								<li>Tidak menggunakan atau menyalahgunakan Layanan Kledo dengan cara apapun yang dapat mengganggu kemampuan pengguna lain untuk menggunakan Layanan atau Website;</li>
								<li>Tidak mencoba untuk mendapatkan akses yang tidak sah kepada materi apapun selain yang sudah dinyatakan dengan jelas bahwa Anda telah mendapatkan izin untuk mengakses-nya, atau untuk mengakses sistem komputer kami dimana Layanan-nya di <em>host</em>;</li>
								<li>Tidak mengirimkan, atau memasukan kedalam Website: file apapun yang dapat merusak alat komputer atau <em>software</em> orang lain, bahan-bahan yang menghina, atau materi atau Data yang melanggar hukum apapun (termasuk Data atau materi lainnya yang terlindungi oleh hak cipta atau rahasia dagang dimana Anda tidak memiliki hak untuk memakainya);</li>
								<li>Tidak mencoba untuk mengubah, menyalin, meniru, membongkar, atau melakukan reverse engineering untuk program komputer apapun yang dipakai untuk memberi Layanan Kledo, atau untuk menggunakan Website diluar penggunaan yang diperlukan dan dimaksudkan.</li>
							</ol>
						</li>
						<li><b>Batasan Penggunaan: </b>Penggunaan Layanan Kledo mungkin dapat dibatasi melingkup, tapi tidak terbatas kepada volume transaksi bulanan dan jumlah proses yang Anda diizinkan untuk gunakan dengan memanggil kepada programming interface aplikasi Kledo. Bila ada, batasan-batasan tersebut akan ditentukan dan dicantumkan pada Layanan yang terkait.</li>
						<li><b>Syarat Komunikasi: </b>Sebagai syarat pada Ketentuan ini, jika Anda memakai alat komunikasi apapun yang tersedia melalui Website (seperti <em>forum</em> apapun atau ruang <em>chat</em>), Anda setuju untuk memakai alat-alat komunikasi tersebut hanya untuk tujuan yang sah. Anda tidak boleh memakai alat-alat komunikasi tersebut untuk memasang atau menyebarkan materi apapun yang tidak terkait dengan pemakaian Layanan-nya, termasuk (tapi tidak terbatas dengan): menawarkan barang dan jasa untuk dijual, <em>e-mail</em> komersial yang tidak diminta atau diinginkan, file yang dapat merusak alat komputer atau software orang lain, bahan-bahan yang mungkin dapat menghina pengguna Layanan atau Website yang lain, atau materi yang melanggar hukum apapun (termasuk materi yang terlindungi oleh hak cipta atau rahasia dagang dimana Anda tidak memiliki hak untuk memakainya).Saat Anda melakukan komunikasi dalam bentuk apapun di Website, Anda menjamin bahwa Anda diperbolehkan untuk membuat komunikasi tersebut. Kledo tidak berkewajiban untuk memastikan bahwa komunikasi pada Website adalah sah dan benar, atau bahwa mereka terkait hanya pada penggunaan Layanan. Kledo berhak untuk menghapus komunikasi apapun setiap saat atas kebijakannya sendiri.</li>
						<li><b>Tanggung Jawab Ganti Rugi: </b>Anda membebaskan Kledo dari semua: tuntutan, gugatan, biaya kerugian, kerusakan, dan kehilangan yang timbul sebagai hasil dari pelanggaran Anda kepada Syarat dan Ketentuan yang tertera di Perjanjian ini, atau kewajiban apapun yang mungkin Anda punya kepada Kledo, termasuk (tapi tidak terbatas kepada) biaya apapun yang berkaitan kepada perolehan Biaya Akses apapun yang sudah jatuh tempo tetapi belum Anda bayar.</li>
					</ol>
					<br>
					<h3 id="kerahasian-privasi">5. Kerahasiaan dan Privasi</h3 >
					<ol>
						<li><b>Kerahasiaan: </b>Masing-masing pihak berjanji untuk menjaga kerahasiaan semua Informasi Rahasia pihak lainnya sehubungan dengan ketentuan ini. Setiap pihak TIDAK AKAN, tanpa persetujuan tertulis dari pihak yang lain, mengungkapkan atau memberi Informasi Rahasia kepada siapapun, atau menggunakannya untuk kepentingan sendiri, selain sebagaimana dimaksud oleh Ketentuan ini.Kewajiban masing-masing pihak dalam ketentuan ini akan bertahan walaupun setelah pemutusan Ketentuan ini.Ketentuan-ketentuan pasal tidak berlaku untuk informasi yang:
							<ol>
								<li>Telah menjadi pengetahuan umum selain karena pelanggaran ketentuan ini;</li>
								<li>Diterima dari pihak ketiga yang dengan secara sah memperolehnya, dan tidak mempunyai kewajiban untuk membatasi pengungkapannya;</li>
								<li>Dikembangkan dengan sendiri tanpa akses kepada Informasi Rahasia.</li>
							</ol>
						</li>
						<li><b>Privasi: </b>Kledo memiliki dan mempertahankan kebijakan privasi yang menjelaskan dan menetapkan kewajiban para pihak untuk menghormati informasi pribadi. Anda disarankan membaca kebijakan privasi kami di <a href="https://kledo.com/privacy-policy/">kledo.com/privacy-policy/</a>, dan Anda akan dianggap sudah menyetujui kebijakan itu saat Anda menyetujui ketentuan ini.</li>
					</ol>
					<br>
					<h3 id="intelectual-property">6. Intellectual Property</h3>
					<ol>
						<li>Kepemilikan dan semua Hak Kekayaan Intelektual yang didapat pada Layanan, Website, dan dokumentasi apapun yang terkait dengan Layanan tetap menjadi hak milik Kledo.</li>
						<li>Kepemilikan dan semua Hak Kekayaan Intelektual yang terdapat di Data tetap menjadi hak milik Anda. Akan tetapi, akses kepada Data Anda bergantung pada pelunasan Biaya Akses Kledo saat tagihan jatuh tempo. Anda memberi izin kepada Kledo untuk memakai, menyalin, mengirim, menyimpan, dan melakukan back-up untuk informasi dan Data Anda dengan maksud dan tujuan memberi Anda akses kepada dan agar dapat menggunakan Layanan Kledo, atau untuk tujuan lainnya yang terkait dengan penyediaan layanan kami untuk Anda.</li>
						<li>Anda sangat disarankan untuk menyimpan salinan untuk semua Data yang Anda masukan ke dalam Layanan Kledo. Kledo mematuhi kebijakan dan menjalani prosedur terbaik untuk mencegah kehilangan data, termasuk rutinitas sistem harian untuk back-up data, tetapi tidak membuat jaminan apapun bahwa tidak akan pernah adanya kehilangan Data. Kledo dengan jelas mengesampingkan tanggung jawab untuk setiap kehilangan Data dengan sebab apapun.</li>
					</ol>
					<br>
					<h3 id="jaminan-pengakuan">7. Jaminan dan Pengakuan</h3>
					<ol>
						<li>Anda menjamin bahwa, jika Anda mendaftar untuk menggunakan Layanan atas nama orang lain, Anda memiliki wewenang untuk menyetujui Ketentuan-ketentuan ini atas nama orang tersebut, dan menyetujui bahwa dengan mendaftar untuk memakai Layanan Kledo, Anda mengikat orang yang Anda atas namakan untuk, atau dengan niat untuk, membuat tindakan atas nama mereka kepada setiap kewajiban manapun yang Anda telah setujui pada Ketentuan-ketentuan ini, tanpa membatasi kewajiban Anda sendiri kepada ketentuannya.</li>
						<li><strong>Anda mengakui bahwa:</strong>
							<ol>
								<li>Anda memiliki wewenang untuk menggunakan Layanan dan Website, dan untuk mengakses informasi dan Data yang Anda masukan kedalam Website, termasuk informasi atau Data apapun yang dimasukan kedalam Website oleh siapapun yang Anda telah beri kewenangan untuk menggunakan Layanan Kledo.Anda juga berwenang untuk mengakses informasi dan data yang sudah di proses, yang disediakan untuk Anda melalui penggunaan Anda pada Website dan Layanan kami (maupun informasi dan Data itu Anda miliki sendiri atau milik orang lain).</li>
								<li>Kledo tidak bertanggung jawab kepada siapapun selain Anda, dan tidak ada maksud apapun dalam Perjanjian ini untuk memberi manfaat kepada siapapun selain Anda. Jika Anda memakai Layanan atau mengakses Website atas nama atau untuk manfaat seseorang selain Anda (maupun organisasi berbadan hukum atau tidak, atau lainnya), Anda menyutujui bahwa:
									<ol>
										<li>Anda bertanggung jawab untuk memastikan bahwa Anda memiliki hak untuk melakukannya;</li>
										<li>Anda bertanggung jawab atas memberi wewenang kepada siapapun yang Anda berikan akses kepada informasi atau Data, dan Anda menyutujui bahwa Kledo tidak memiliki tanggung jawab untuk menyediakan siapapun akses kepada informasi atau Data tersebut tanpa otorisasi Anda, dan boleh menunjukan permohonan apapun untuk mendapatkan informasi kepada Anda untuk di layani;</li>
										<li>Anda membebaskan Kledo dari klaim apapun atau kehilangan yang terkait pada: Penolakan Kledo untuk menyediakan akses pada siapapun kepada informasi atau Data Anda sesuai dengan ketentuan ini; Penyediaan informasi atau Data oleh Kledo kepada siapapun berdasarkan otorisasi Anda.</li>
									</ol>
								</li>
								<li>Penyediaan, akses kepada, dan pemakaian Layanan Kledo tersedia sebagaimana adanya dan pada resiko Anda sendiri.</li>
								<li>Kledo tidak menjamin bahwa penggunaan Layanan tidak akan pernah terganggu atau bebas dari kesalahan. Di antara lain, operasi dan ketersediaan sistem yang digunakan untuk mengakses Layanan, termasuk layanan telpon umum, jaringan komputer dan internet, bisa susah diprediksi dan mungkin bisa dari waktu ke waktu menggangu atau mencegah akses kepada Layanan. Kledo dengan bagaimanapun tidak bertanggung jawab atas gangguan tersebut, atau pencegahan akses kepada penggunaan Layanan.</li>
								<li>Kledo bukanlah akuntan Anda, dan penggunaan Layanan tidak merupakan penerimaan nasihat akunting. Jika Anda memiliki pertanyaan akunting, harap hubungi seorang akuntan.</li>
								<li>Untuk menentukan bahwa Layanan kami memenuhi keperluan bisnis Anda dan bisa digunakan sesuai dengan tujuan adalah tanggung jawab Anda sendiri.</li>
								<li>Anda tetap memiliki tanggung jawab untuk mematuhi semua undang-undang akunting, perpajakan, dan lainnya yang berlaku. Menjadi tanggung jawab Anda untuk memeriksa bahwa penyimpanan dan akses kepada Data Anda melalui Layanan dan Website tetap mematuhi undang-undang yang berlaku kepada Anda (termasuk undang-undang apapun yang membutuhkan Anda untuk menyimpan arsip).</li>
							</ol>
						</li>
						<li>Kledo tidak memberi jaminan untuk Layanan-nya. Tanpa mengabaikan ketentuan di atas, Kledo tidak menjamin bahwa Layanan kami akan memenuhi keperluan Anda, atau bahwa akan sesuai untuk tujuan yang Anda niatkan. Untuk menghindari keraguan, semua ketentuan atau jaminan yang bisa diartikan dikecualikan sejauh yang diijinkan oleh hukum, termasuk (tanpa batasan) jaminan penjualan, kesesuaian untuk tujuan, dan tanpa pelanggaran.</li>
						<li>Anda menjamin dan menunjukkan bahwa Anda sedang memperoleh hak untuk mengakses dan menggunakan Layanan untuk tujuan bisnis dan bahwa, sampai batas maksimum yang diizinkan oleh hukum, jaminan konsumen berdasarkan hukum atau undang-undang yang dimaksudkan untuk melindungi konsumen non-bisnis di yurisdiksi manapun tidak berlaku untuk penyediaan Layanan, Website, atau ketentuan ini.</li>
					</ol>
					<br>
					<h3 id="batasan-kewajiban">8. Batasan Kewajiban</h3>
					<ol>
						<li>Sampai batas maksimal yang diizinkan oleh hukum, Kledo mengecualikan semua kewajiban dan tanggung jawab kepada Anda (atau orang lain manapun) dalam kontrak, gugatan kesalahan(termasuk kelalaian), atau selainnya, untuk kehilangan apapun (termasuk kehilangan informasi, Data, keuntungan, dan tabungan) atau kerusakan yang diakibatkan, secara langsung atau tidak langsung, dari penggunaan apapun, atau ketergantungan kepada, Layanan atau Website.</li>
						<li>Jika Anda mengalami kerugian atau kerusakan apapun karena kelalaian atau kegagalan Kledo untuk memenuhi ketentuan ini, klaim apapun dari Anda kepada Kledo yang timbul dari kelalaian atau kegagalan Kledo akan dibataskan mengenai satu kejadian, atau serangkaian kejadian yang terhubung, kepada Biaya Akses yang sudah Anda lunaskan dalam 12 bulan sebelumnya.</li>
						<li>Jika Anda tidak puas dengan Layanan-nya, jalur penyelesaian tunggal dan eksklusif Anda adalah untuk menghentikan ketentuan ini sesuai dengan pasal 8.</li>
					</ol>
					<br>
					<h3 id="pemutusan-kontrak">9. Pemutusan Kontrak</h3>
					<ol>
						<li><b>Kebijakan Masa Percobaan: </b>Saat Anda pertama mendaftar untuk mengakses Layanan, Anda bisa mengevaluasi Layanan-nya dengan ketentuan untuk masa percobaan yang sudah di tentukan, tanpa kewajiban untuk melanjutkan penggunaan Layanan-nya. Jika Anda memilih untuk melanjutkan penggunaan Layanan setelah masa percobaan selesai, Anda akan mulai ditagih dari hari sama dimana Anda memasuki informasi untuk penagihan Anda. Jika Anda memilih untuk berhenti menggunakan Layanan-nya, Anda bisa menghapus organisasi Anda.</li>
						<li>Kledo tidak menyediakan pengembalian uang untuk periode prabayar yang tersisa pada langganan Biaya Akses Anda.</li>
						<li>Ketentuan ini akan berlaku untuk periode yang dicakup oleh Biaya Akses yang telah atau dapat dibayar pada pasal 4 ayat 1. Pada masa akhir setiap periode penagihan, Ketentuan ini akan berlanjut secara otomatis untuk periode selanjutnya untuk jangka watu yang sama, asalkan Anda terus membayar Biaya Akses yang sudah ditentukan saat jatuh tempo, kecuali salah satu pihak mengakhiri Ketentuan ini dengan pemberitahuan kepada pihak lainnya setidaknya 30 hari sebelum akhir periode pembayaran yang bersangkutan.</li>
						<li><strong>Interval pembayaran: </strong>Interval pembayaran bersifat otomatis dan mengikat seperti yang dijelaskan pada pasal 4 ayat 1. Jika Anda memutuskan untuk tidak melakukan pembayaran hingga waktu yang ditentukan dengan alasan apapun, kami menganggap Anda sudah berhenti menggunakan layanan kami. Jika ingin melanjutkan proses berlangganan di kemudian hari dan ingin mengakses data sebelumnya, maka Anda harus melakukan pembayaran terhitung dari bulan Anda behenti menggunakan layanan kami hingga pada bulan melanjutkan proses pengaktifan layanan kembali.</li>
						<li>Pelanggaran
							<ol>
								<li>Jika Anda melanggar apapun dari Ketentuan ini (termasuk, tapi tidak terbatas dengan, tidak membayar Biaya Akses apapun) dan tidak menyelesaikan masalah pelanggaran dalam 14 hari setelah menerima pemberitahuan pelanggaran tersebut jika permasalahan pelanggarang tersebut bisa di selesaikan;</li>
								<li>Jika Anda melanggar apapun dari Ketentuan ini dan pelanggaran itu tidak bisa di selesaikan (termasuk, tapi tidak terbatas dengan) pelanggaran apapun mengenai pasal 3 ayat 4 atau kegagalan untuk membayar Biaya Akses yang sudah melewati tanggal jatuh tempo lebih dari 30 hari;</li>
								<li>Jika Anda atau bisnis Anda bangkrut, atau sedang melewati proses untuk mengakhiri keberadaan organisasi, Kledo bisa mengambil salah satu atau semua tindakan berikut:
									<ol>
										<li>Mengakhiri Perjanjian ini dan penggunaan Anda untuk Layanan dan Website kami;</li>
										<li>Menunda akses Anda kepada Layanan dan Website Kledo untuk periode yang tidak menentu;</li>
										<li>Menunda akses atau mengakhiri akses kepada semua Data atau Data apapun;</li>
									</ol>
								</li>
								<li>Pemutusan Ketentuan ini tidak mengurangi hak dan kewajiban para pihak yang masih harus dibayar sampai dengan tanggal terminasi. Pada pengakhiran Perjanjian ini Anda akan tetap menanggung biaya yang masih harus dibayar dan jumlah yang jatuh tempo untuk pembayaran sebelum atau setelah pengakhiran, dan segera berhenti menggunakan Layanan dan Website kami.</li>
							</ol>
						</li>
					</ol>
					<br>
					<h3 id="ketentuan-umum-lainnya">10. Ketentuan Umum Lainnya</h3>
					<ol>
						<li>Syarat dan Ketentuan ini, bersama dengan Kebijakan Privasi Kledo dan ketentuan dari pemberitahuan atau instruksi yang diberikan kepada Anda dibawah Syarat dan Metentuan ini menggantikan dan menghapus semua perjanjian sebelumnya, representasi (baik lisan maupun tulisan), dan pemahaman, dan merupakan keseluruhan perjanjian antara Anda dan Kledo yang berhubungan dengan Layanan dan hal lainnya yang dibahas dalam Ketentuan ini.</li>
						<li>Jika salah satu pihak melepaskan tuntutan dari pelanggaran apapun pada Ketentuan ini, ini tidak akan melepaskan mereka dari tuntutan pelanggaran lainnya. Pelepasan dari tuntuntan tidak efektif kecuali dibuat secara tulisan.</li>
						<li>Para pihak tidak harus bertanggung jawab atas keterlambatan atau kegagalan dalam untuk menyelesaikan kewajibannya dibawah Ketentuan ini jika keterlambatan atau kegagalannya adalah karena sebab apapun yang di luar kendali. Ayat ini tidak berlaku untuk kewajiban pembayaran uang apapun.</li>
						<li>Anda tidak dapat mengalihkan atau mentransfer hak kepada orang lain tanpa persetujuan tertulis dari Kledo.</li>
						<li>Apabila terjadi perselisihan antara kedua belah pihak, akan coba diselesaikan secara musyawarah terlebih dahulu untuk mencapai mufakat. Apabila dengan cara tersebut tidak tercapai kata sepakat, maka kedua belah pihak sepakat untuk menyelesaikan permasalahan tersebut dilakukan melalui prosedur hukum dengan memilih kedudukan hukum Republik Indonesia.</li>
						<li>Setiap pemberitahuan yang diberikan berdasarkan Persyaratan ini oleh satu pihak ke yang lain harus dilakukan secara tertulis melalui email dan akan dianggap telah diberikan pada saat transmisi dilakukan. Pemberitahuan kepada Kledo harus dikirim ke hello@kledo.com atau ke alamat email lainnya yang diberitahukan kepada Anda oleh Kledo. Pemberitahuan kepada Anda akan dikirim ke alamat email yang Anda berikan saat membuat akses Anda pada Layanan kami.</li>
						<li>Subscription adalah pembayaran berulang yang dibayar di muka untuk mengkonsumsi jasa aplikasi akuntansi yang disediakan kledo.com . Setelah subscription telah dibeli, anda bisa membatalkan setiap saat tanpa biaya tambahan kecuali yang sudah dibayarkan. Pembatalan membutuhkan paling lama 31 hari sebelum efektif.</li>
					</ol>
					<br>
					<style>
						.post-content > *:not(h2):not(h3):not(h4) {
							text-align: justify;
							word-break: break-word;
							word-wrap: break-word;
							line-height: 2;
						}
						.post-content h2 {
							font-size: calc(1.4rem + .9vw);
						}
						.post-content h3 {
							font-size: calc(1.3rem + .6vw);
						}
						.post-content > h2 {
							text-align: left;
							margin-bottom: 20px;
						}
						.post-content > h2 ~ p {
							margin-left: 27px;
						}
						.post-content > h2 ~ ol {
							margin-left: 27px;
						}
						.post-content ol {
							padding-inline-start: 20px;
							padding-left: 20px;
						}
						.post-content ol li li {
							list-style: lower-latin;
							margin-bottom: 0;
						}
						.post-content table td {
							vertical-align: top;
						}
					</style>
				</div>
			</div>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>