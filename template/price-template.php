<?php /* Template Name: Price Template */ ?>

<?php get_header() ?>
	<article id="header" class="bg-theme-1 text-white pt-lg-3">
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						Semua fitur hebat Kledo hanya 129.900rb/bulan
					</h1>
					<p class="fs-6 opacity-7 text-break text-center mt-2 mb-0 mx-md-5">
						Mulai dari invoicing, purchasing, inventori, manajemen aset tetap, laporan akuntansi hingga analisa bisnis semuanya ada.
					</p>
				</div>
			</div>
		</div>
	</article>

	<article class="bg-theme-1 position-relative">
		<div class="container pt-5 pb-3">
			<div class="px-xl-5 mx-xl-3">
				<div class="d-flex justify-content-center font-title mb-5 pb-3 pricing-switch">
					<div class="d-flex bg-white rounded-pill shadow p-2">
						<button type="button" class="btn btn-clear fs-6 position-relative pointer-events-none" onclick="switchPrice('.pricing-switch','.pricing')">
							<span>per <b>Tahun</b></span>
							<div class="position-absolute top left right mt-n4">
								<span class="badge badge-success text-white rounded-pill p-2">5%</span>
							</div>
						</button>
						<div class="custom-control custom-switch custom-switch-lg switch-always-on switch-overlay ml-2" onclick="switchPrice('.pricing-switch','.pricing')">
							<input type="checkbox" class="custom-control-input" id="switchPricing">
							<label class="custom-control-label" for="switchPricing">
								<span class="sr-only">Switch Pricing</span>
							</label>
						</div>
						<button type="button" class="btn btn-clear fs-6" onclick="switchPrice('.pricing-switch','.pricing')">
							<span class="opacity-5">per <b>Bulan</b></span>
						</button>
					</div>
				</div>
				<div class="row no-gutters pricing">
					<div class="col-md-4 mb-4 pb-2 index-1 mt-md-4 pt-md-2">
						<div class="card bg-white border-0 rounded-3 rounded-right-md-0 shadow text-center text-break">
							<div class="card-body py-4 px-lg-4">
								<h3 class="fs-5 lh-2 text-uppercase font-title font-weight-bold my-2">
									Gratis
								</h3>
								<div class="font-title mt-3 mb-4">
									<div class="fs-1 d-flex align-items-center justify-content-center text-warning lh-2 mr-4 mr-lg-0 mr-xl-4 pr-2 pr-md-3">
										<span class="fs-4 opacity-2 text-dark mb-1 mr-1">Rp</span>
										<b>0</b>
										<b class="d-none">0</b>
									</div>
									<div class="opacity-3">selamanya</div>
								</div>
								<div class="separator my-3 mx-auto w-75"></div>
								<ul class="list-group list-group-flush lh-2 mb-0">
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0 d-none d-md-block">&nbsp;</li>
								</ul>
							</div>
							<div class="card-footer bg-warning w-25 border-0 rounded-3 py-1 mt-2 mb-4 mx-auto"></div>
						</div>
					</div>
					<div class="col-md-4 mb-4 pb-2 index-2">
						<div class="card bg-white border-0 rounded-3 shadow text-center text-break">
							<div class="card-body py-4 px-lg-4">
								<h3 class="fs-5 lh-2 text-uppercase font-title font-weight-bold my-2">
									Elit
								</h3>
								<div class="font-title mt-3 mb-4">
									<div class="fs-1 d-flex align-items-center justify-content-center text-theme-1 lh-2 mr-4 mr-lg-0 mr-xl-4">
										<span class="fs-4 opacity-2 text-dark mb-1 mr-1">Rp</span>
										<b>5700</b>
										<b class="d-none">6000</b>
									</div>
									<div class="opacity-3">per bulan</div>
								</div>
								<div class="separator my-3 mx-auto w-75"></div>
								<ul class="list-group list-group-flush lh-2 mb-0">
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
								</ul>
							</div>
							<div class="card-footer bg-theme-1 w-25 border-0 rounded-3 py-1 mt-2 mb-4 mx-auto"></div>
						</div>
					</div>
					<div class="col-md-4 mb-4 pb-2 index-1 mt-md-4 pt-md-2">
						<div class="card bg-white border-0 rounded-3 rounded-left-md-0 shadow text-center text-break">
							<div class="card-body py-4 px-lg-4">
								<h3 class="fs-5 lh-2 text-uppercase font-title font-weight-bold my-2">
									Pro
								</h3>
								<div class="font-title mt-3 mb-4">
									<div class="fs-1 d-flex align-items-center justify-content-center text-info lh-2 mr-4 mr-lg-0 mr-xl-4">
										<span class="fs-4 opacity-2 text-dark mb-1 mr-1">Rp</span>
										<b>4750</b>
										<b class="d-none">5000</b>
									</div>
									<div class="opacity-3">per bulan</div>
								</div>
								<div class="separator my-3 mx-auto w-75"></div>
								<ul class="list-group list-group-flush lh-2 mb-0">
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
								</ul>
							</div>
							<div class="card-footer bg-info w-25 border-0 rounded-3 py-1 mt-2 mb-4 mx-auto"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="position-absolute left right" style="bottom:200px">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-bottom">
				<polygon points="0,0 100,25 100,30 0,30"/>
			</svg>
		</div>
		<div class="position-absolute left right bg-white overlay-bottom" style="height:205px"></div>
	</article>

	<article>
		<div class="container py-5">
			<div class="row mb-3">
				<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
					<h2 class="fs-2 text-break mb-3 text-center">
						Perbandingan Paket
					</h2>
				</div>
			</div>
			<div class="mt-4">
				<div class="collapse collapse-list collapse-compare mx-n4 mx-sm-n5 mx-md-0 px-4 px-sm-5 px-md-0">
					<div class="table-responsive vw-100 w-sm-auto mx-n4 mx-sm-n5 mx-md-0">
						<table class="table table-borderless table-striped position-relative text-center">
							<thead>
								<tr>
									<th><div class="ellipsis line-1 text-left opacity-0">-----------------------------------------</div></th>
									<th><b class="font-title text-uppercase text-warning">Gratis</b></th>
									<th><b class="font-title text-uppercase text-info">Pro</b></th>
									<th><b class="font-title text-uppercase text-theme-1">Elit</b></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="text-left">Pengguna</td>
									<td>1 orang</td>
									<td>3 orang</td>
									<td>5 orang</td>
								</tr>
								<tr>
									<td class="text-left">Laporan Keuangan</td>
									<td>10 (Laba Rugi, Neraca, Arus Kas, dll)</td>
									<td>35 Laporan</td>
									<td>35 Laporan + Konsolidasi</td>
								</tr>
								<tr>
									<td class="text-left">Stok & Inventori</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td>2 Gudang</td>
									<td>5 Gudang</td>
								</tr>
								<tr>
									<td class="text-left">Dukungan Pajak</td>
									<td>PPN</td>
									<td>PPN & PPH</td>
									<td>PPN, PPH + Custom Pajak</td>
								</tr>
								<tr>
									<td class="text-left">Konsolidasi Multi Perusahaan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Pembatasan Hak Akses</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Aset Tetap</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Multi Cabang, Multi Divisi, Multi Outlet, Multi Proyek</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr class="bg-transparent">
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td class="fs-5 font-weight-bold text-left" colspan="5">
										Sales
									</td>
								</tr>
								<tr>
									<td class="text-left">Statistik Penjualan</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Faktur Penjualan</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Pengiriman Penjualan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Pemesanan Penjualan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Penawaran Penjualan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td class="fs-5 font-weight-bold text-left" colspan="5">
										Purchasing
									</td>
								</tr>
								<tr>
									<td class="text-left">Statistik Pembelian</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Faktur Pembelian</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Pengiriman Pembelian</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Pemesanan Pembelian</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Penawaran Pembelian</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td class="fs-5 font-weight-bold text-left" colspan="5">
										Produk
									</td>
								</tr>
								<tr>
									<td class="text-left">Produk Basic</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Produk Paket</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Produk Manufaktur</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Pelacakan Stok Produk</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Stok Opname</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Manajemen Stok Per Gudang</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Konversi Produk & Bahan Baku</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td class="fs-5 font-weight-bold text-left" colspan="5">
										Laporan
									</td>
								</tr>
								<tr>
									<td class="text-left">Pencatatan Biaya</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Hutang Piutang Karyawan</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Tambah Akun COA</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Laporan Neraca, Laba Rugi, Arus Kas</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Laporan Akuntansi Lengkap: Jurnal Umum, Buku Besar, Laporan Pajak, dll</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Laporan Bisnis: Laporan Stok, Laporan Penjualan, Pembelian, Laporan Gudang, dll</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Laporan Konsolidasi Multi Perusahaan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<td class="fs-5 font-weight-bold text-left" colspan="5">
										Print out
									</td>
								</tr>
								<tr>
									<td class="text-left">50 Template Invoice</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Cetak Faktur</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Cetak Kuitansi</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Cetak Faktur Pajak</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Cetak Surat Jalan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Cetak Penawaran</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Cetak Pemesanan</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">File Attachment</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Export Excel</td>
									<td class="text-dark opacity-2"><i class="bi-x-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
								<tr>
									<td class="text-left">Export PDF</td>
									<td class="text-warning"><i class="bi-check-lg"></i></td>
									<td class="text-info"><i class="bi-check-lg"></i></td>
									<td class="text-theme-1"><i class="bi-check-lg"></i></td>
								</tr>
							</tbody>
							<tfoot class="lock">
							</tfoot>
						</table>
					</div>
				</div>
				<div class="d-flex justify-content-center flex-column flex-md-row">
					<button type="button" class="btn btn-danger rounded-pill text-uppercase" data-toggle="collapse" aria-expanded="false" data-target=".collapse-compare" onclick="this.classList.add('d-none')">
						<div class="d-flex align-items-center">
							<span class="fs-7 mx-2">
								Lihat <b>Selengkapnya</b>
							</span>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="32" height="32">
								<path fill="#292D32" opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
								<path fill="#fff" d="M15.5295 11.97C15.2395 11.68 14.7595 11.68 14.4695 11.97L12.7495 13.69V8.5C12.7495 8.09 12.4095 7.75 11.9995 7.75C11.5895 7.75 11.2495 8.09 11.2495 8.5V13.69L9.52945 11.97C9.23945 11.68 8.75945 11.68 8.46945 11.97C8.17945 12.26 8.17945 12.74 8.46945 13.03L11.4695 16.03C11.6195 16.18 11.8095 16.25 11.9995 16.25C12.1895 16.25 12.3795 16.18 12.5295 16.03L15.5295 13.03C15.8195 12.74 15.8195 12.26 15.5295 11.97Z"/>
							</svg>
						</div>
					</button>
				</div>
				<style>
					@font-face {
						font-family: bootstrap-icons;
						font-display: swap;
						src: url(https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.5.0/font/fonts/bootstrap-icons.woff2?856008caa5eb66df68595e734e59580d) format("woff2"),url(https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.5.0/font/fonts/bootstrap-icons.woff?856008caa5eb66df68595e734e59580d) format("woff")
					}
					[class*=" bi-"]:before, [class^=bi-]:before {
						display: inline-block;
						font-family: bootstrap-icons!important;
						font-style: normal;
						font-weight: 400!important;
						font-variant: normal;
						text-transform: none;
						line-height: 1;
						vertical-align: -0.125em;
						-webkit-font-smoothing: antialiased;
						-moz-osx-font-smoothing: grayscale;
					}
					.bi-check-lg:before {
						content: "\f633";
					}
					.bi-x-lg:before {
						content: "\f659";
					}
					.collapse-list:not(.show) {
						display: block;
						overflow: hidden;
						height: 10rem;
						position: relative;
					}
					.collapse-list:not(.show):after {
						content: '';
						display: block;
						width: 100%;
						height: 5rem;
						position: absolute;
						bottom: 0;
						left: 0;
						background: rgb(255,255,255);
						background: linear-gradient(0deg, rgba(255,255,255,1) 25%, rgba(255,255,255,0) 100%);
					}
					.collapse-list-parent [aria-expanded="true"] {
						transform: rotate(180deg);
					}
					.collapse-compare:not(.show) {
						height: 730px;
					}
					.table tr th:first-child,
					.table tr td:first-child {
						min-width: 175px;
					}
					.table tr th:not(:first-child),
					.table tr td:not(:first-child) {
						width: 15%;
						min-width: 250px;
					}
					.table-striped tbody tr:nth-of-type(odd) {
						background-color: transparent;
					}
					.table-striped tbody tr:nth-of-type(even) {
						background-color: rgba(0,0,0,.015);
					}
					.table tbody + tbody tr:first-child td {
						padding-top: 1.5rem;
					}
					@media (max-width: 575px) {
						.table tr th:first-child,
						.table tr td:first-child {
							padding-left: 2rem;
						}
						.table tr th:last-child,
						.table tr td:last-child {
							padding-right: 2rem;
						}
					}
					@media (min-width: 576px) {
						.w-sm-auto {
							width: auto !important;
						}
					}
					.table-responsive {
						-ms-overflow-style: none;
						scrollbar-width: none;
					}
					.table-responsive::-webkit-scrollbar {
						display: none;
					}
					.table-responsive .lock,
					.table-responsive .lock:after {
						position: fixed;
						top: calc(59px - 5px);
						width: 100%;
					}
					.table-responsive .lock {
						padding-top: 5px;
						overflow: auto;
						z-index: 1;
						-ms-overflow-style: none;
						scrollbar-width: none;
					}
					.table-responsive .lock::-webkit-scrollbar {
						display: none;
					}
					.table-responsive .lock:after {
						content: '';
						left: 0;
						right: 0;
						margin: auto;
						height: calc(46px + 5px);
						background-color: rgba(255,255,255,.95);
						-webkit-backdrop-filter: blur(.25rem);
						backdrop-filter: blur(.25rem);
						z-index: -1;
					}
					.table-responsive .lock,
					.table-responsive .lock:after {
						top: calc(72px - 5px);
					}
					@media (min-width: 588px) {
						.table-responsive .lock {
							width: 588px;
						}
					}
					@media (min-width: 768px) {
						.table-responsive .lock,
						.table-responsive .lock:after {
							top: calc(82px - 5px);
						}
						.table-responsive .lock {
							width: 672px;
						}
					}
					@media (min-width: 992px) {
						.table-responsive .lock {
							width: 912px;
						}
					}
					@media (min-width: 1200px) {
						.table-responsive .lock {
							width: 1092px;
						}
					}
					@media (min-width: 1400px) {
						.table-responsive .lock {
							width: 1292px;
						}
					}
					.customize-support .table-responsive .lock,
					.customize-support .table-responsive .lock:after {
						top: calc(59px - 5px + 46px);
					}
					@media (min-width: 768px) {
						.customize-support .table-responsive .lock,
						.customize-support .table-responsive .lock:after {
							top: calc(82px - 5px + 32px);
						}
					}
				</style>
				<script>
					document.addEventListener('DOMContentLoaded', function() {
						$('.table-responsive tfoot').html($('.table-responsive thead tr').clone()).hide()
						$('.table-responsive').on('scroll', function() {
							$('.table-responsive .lock').scrollLeft( $(this).scrollLeft() )
						})
						$(window).on('scroll', function() {
							let screen = $(window).scrollTop()
								parent = $('.collapse-compare')
								child = $('.table-responsive')
								reachTop = child.offset().top - 90
								reachBottom = child.offset().top + parent.innerHeight() - 185
								thead = $('.table-responsive thead')
								tfoot = $('.table-responsive .lock')
							if ( screen <= reachTop ) {
								tfoot.fadeOut(200)
								thead.removeClass('opacity-0')
							} else if ( screen >= reachBottom ) {
								tfoot.fadeOut(200)
								thead.removeClass('opacity-0')
							} else {
								tfoot.fadeIn(200)
								thead.addClass('opacity-0')
							}
						})
					})
				</script>
			</div>
		</div>
	</article>

	<article class="bg-primary text-white my-4">
		<div class="container py-5">
			<div class="row mt-2 mb-3">
				<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
					<h2 class="fs-2 text-break mb-3 text-center">
						Addons
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-6 mb-4">
					<div class="card p-2 p-md-3 rounded-3 border-0 shadow text-dark">
						<div class="card-body">
							<div class="d-md-flex align-items-center justify-content-between text-center">
								<div class="font-title h1 mb-3 mb-md-0">
									Rp <strong>19.900</strong>
								</div>
								<a class="btn btn-success rounded-pill py-2 px-3 my-1 my-md-0" href="#">
									Tambahan Gudang
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 mb-4">
					<div class="card p-2 p-md-3 rounded-3 border-0 shadow text-dark">
						<div class="card-body">
							<div class="d-md-flex align-items-center justify-content-between text-center">
								<div class="font-title h1 mb-3 mb-md-0">
									Rp <strong>19.900</strong>
								</div>
								<a class="btn btn-success rounded-pill py-2 px-3 my-1 my-md-0" href="#">
									Tambahan User
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="container py-5">
			<div class="row mb-4">
				<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
					<h2 class="fs-2 text-break mb-3 text-center">
						Semua Paket Berbayar Termasuk <br class="d-none d-md-inline-block">Fitur Berikut
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Keamanan</h3>
					<p class="opacity-5 text-break">Seluruh komunikasi dengan server dienkripsi dengan 256-bit SSL encryption. Serta berbagai fitur keamanan lainnya.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Integrasi & API</h3>
					<p class="opacity-5 text-break">Hubungkan software yang telah kamu gunakan dengan Kledo melaluI REST APIs yang kami sediakan.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Akses Dari Mana Saja</h3>
					<p class="opacity-5 text-break">iOS, Android, Windows, Mac semua bisa untuk mengakses Kledo. Tak perlu khawatir!</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Undang Tim</h3>
					<p class="opacity-5 text-break">Undang karyawan, staff, ataupun akuntan untuk ikut berkolaborasi menggunakan Kledo.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Penyusutan Aset Otomatis</h3>
					<p class="opacity-5 text-break">Perhitungan penyusutan aset tetap akan dilakukan secara otomatis oleh Kledo. Baca Selengkapnya.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Pembatasan Hak Akses</h3>
					<p class="opacity-5 text-break">Atur hak ases untuk setiap karyawan atau akuntan di perusahaanmu. Pastikan hanya memberi hak ases sesuai kebutuhan.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Statistik Bisnis</h3>
					<p class="opacity-5 text-break">Dapatkan grafik statistik secara realtime untuk memantau performa bisnismu setiap saat. Baca Selengkapnya.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Perhitungan Pajak</h3>
					<p class="opacity-5 text-break">Pajakmu akan otomatis dikalkulasi secara realtime oleh Kledo. Tak perlu lagi ribet hitung-hitung pajak.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Kustomisasi Pajak</h3>
					<p class="opacity-5 text-break">Buat pajak sesuai kebutuhan, berapa persen potongannya dan sebagainya.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Kustom Akun Akuntansi</h3>
					<p class="opacity-5 text-break">Tambah ubah dan hapus akun akuntansi (Chart of Accounts) sesuai kebutuhan bisnismu.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Buat Faktur dengan 1 Klik</h3>
					<p class="opacity-5 text-break">Buat faktur dari penawaran penjualan maupun purchase order hanya dengan 1 klik. Baca Selengkapnya.</p>
				</div>
				<div class="col-md-6 mb-3 pb-3">
					<h3 class="fs-4 text-break mb-2">Import Data Masal Excel</h3>
					<p class="opacity-5 text-break">Import data invoice, purchase order, produk dan sebagainya dengan file excel, bisa ratusan data dalam satu waktu.</p>
				</div>
			</div>
		</div>
	</article>

	<article>
		<div class="container py-5 position-relative text-center">
			<div class="row">
				<div class="col-12">
					<h2 class="fs-2 text-break mb-3">
						Ada pertanyaan?
					</h2>
					<p class="fs-6 opacity-6 mb-3 pb-1">
						Silakan hubungi kami melalui WhatsApp
					</p>
					<a class="btn btn-success rounded-pill py-2 pl-3 pr-4 lh-1 fs-6 font-title text-uppercase ws-nowrap" href="https://wa.me/6282383334000&text=Hallo, Saya mau tanya tentang Kledo.">
						<div class="d-flex align-items-center">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="currentColor" class="m-1">
								<path d="M12.04 2C6.58 2 2.13 6.45 2.13 11.91C2.13 13.66 2.59 15.36 3.45 16.86L2.05 22L7.3 20.62C8.75 21.41 10.38 21.83 12.04 21.83C17.5 21.83 21.95 17.38 21.95 11.92C21.95 9.27 20.92 6.78 19.05 4.91C17.18 3.03 14.69 2 12.04 2M12.05 3.67C14.25 3.67 16.31 4.53 17.87 6.09C19.42 7.65 20.28 9.72 20.28 11.92C20.28 16.46 16.58 20.15 12.04 20.15C10.56 20.15 9.11 19.76 7.85 19L7.55 18.83L4.43 19.65L5.26 16.61L5.06 16.29C4.24 15 3.8 13.47 3.8 11.91C3.81 7.37 7.5 3.67 12.05 3.67M8.53 7.33C8.37 7.33 8.1 7.39 7.87 7.64C7.65 7.89 7 8.5 7 9.71C7 10.93 7.89 12.1 8 12.27C8.14 12.44 9.76 14.94 12.25 16C12.84 16.27 13.3 16.42 13.66 16.53C14.25 16.72 14.79 16.69 15.22 16.63C15.7 16.56 16.68 16.03 16.89 15.45C17.1 14.87 17.1 14.38 17.04 14.27C16.97 14.17 16.81 14.11 16.56 14C16.31 13.86 15.09 13.26 14.87 13.18C14.64 13.1 14.5 13.06 14.31 13.3C14.15 13.55 13.67 14.11 13.53 14.27C13.38 14.44 13.24 14.46 13 14.34C12.74 14.21 11.94 13.95 11 13.11C10.26 12.45 9.77 11.64 9.62 11.39C9.5 11.15 9.61 11 9.73 10.89C9.84 10.78 10 10.6 10.1 10.45C10.23 10.31 10.27 10.2 10.35 10.04C10.43 9.87 10.39 9.73 10.33 9.61C10.27 9.5 9.77 8.26 9.56 7.77C9.36 7.29 9.16 7.35 9 7.34C8.86 7.34 8.7 7.33 8.53 7.33Z"/>
							</svg>
							<span class="mx-1">
								0823 8333 4000
							</span>
						</div>
					</a>
				</div>
			</div>
			<img class="obj-fit-cover obj-position-top position-absolute right bottom mb-n5 mr-5 d-none d-md-block" loading="lazy" width="275" height="300" src="upload/woman.png">
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>