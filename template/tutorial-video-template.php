<?php /* Template Name: Tutorial Video Template */ ?>

<?php get_header() ?>

	<article id="header" class="bg-theme-1 text-white pb-md-4 pb-lg-5 pt-lg-3">
		<div class="container pt-5 pb-5 pb-md-3">
			<h1 class="fs-1 text-break text-center mb-0">
				Video Tutorial
			</h1>
			<p class="fs-6 opacity-7 text-break text-center mt-2 mb-0">
				Cari yang ingin kamu pelajari dengan submit form di bawah. <br class="d-none d-md-inline-block">
				Jika tidak menemukan yang kamu cari, silahkan hubungi tim Hebat Kledo yah :)
			</p>
			<div class="row">
				<div class="col-md-9 col-lg-8 col-xl-7 mx-auto">
					<div class="bg-white p-3 rounded-pill shadow mt-4" id="findTutorial">
						<form class="form-inline" spellcheck="false" autocomplete="off">
							<div class="d-flex w-100">
								<label class="sr-only" for="input-keyword">Kata Kunci</label>
								<input type="text" name="keyword" class="form-control bg-transparent border-0 w-100 pl-3" id="input-keyword" placeholder="Apa yang ingin Anda cari?">
								<div class="d-flex">
									<button type="reset" class="btn btn-clear text-danger p-0 mr-1" style="display:none">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="25" height="25" fill="var(--theme-1)">
											<path d="M17.7,18.8L5.2,6.3C5,6,5,5.5,5.2,5.2C5.5,5,6,5,6.3,5.2l12.4,12.4c0.3,0.3,0.3,0.8,0,1.1C18.5,19,18,19,17.7,18.8z"/>
											<path d="M5.2,17.7L17.7,5.2C18,5,18.5,5,18.8,5.2C19,5.5,19,6,18.8,6.3L6.3,18.8C6,19,5.5,19,5.2,18.8C5,18.5,5,18,5.2,17.7z"/>
										</svg>
										<span class="sr-only">Reset</span>
									</button>
									<button type="submit" class="btn btn-clear py-0 opacity-5">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="var(--black)">
											<path d="M11.5 21.75C5.85 21.75 1.25 17.15 1.25 11.5C1.25 5.85 5.85 1.25 11.5 1.25C17.15 1.25 21.75 5.85 21.75 11.5C21.75 17.15 17.15 21.75 11.5 21.75ZM11.5 2.75C6.67 2.75 2.75 6.68 2.75 11.5C2.75 16.32 6.67 20.25 11.5 20.25C16.33 20.25 20.25 16.32 20.25 11.5C20.25 6.68 16.33 2.75 11.5 2.75Z"/>
											<path d="M22.0004 22.7499C21.8104 22.7499 21.6204 22.6799 21.4704 22.5299L19.4704 20.5299C19.1804 20.2399 19.1804 19.7599 19.4704 19.4699C19.7604 19.1799 20.2404 19.1799 20.5304 19.4699L22.5304 21.4699C22.8204 21.7599 22.8204 22.2399 22.5304 22.5299C22.3804 22.6799 22.1904 22.7499 22.0004 22.7499Z"/>
										</svg>
										<span class="sr-only">Search</span>
									</button>
								</div>
							</div>
						</form>
					</div>
					<div class="position-absolute text-center mt-3" id="resultTutorial" style="width:calc(100% - 2rem);display:none">
						Ditemukan <span></span> hasil pencarian
					</div>
				</div>
			</div>
		</div>
	</article>

	<article>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1)" class="overlay-top overlay-flip-y">
			<polygon points="100,30 0,30 0,25 100,0"/>
		</svg>
		<div class="container py-5">
			<div class="row">
				<div class="col-md-4 col-lg-3">
					<form spellcheck="false" class="pills-responsive pt-2 pb-2 pb-md-3 mt-n2 mb-3 mb-md-4 position-sticky">
						<div class="mb-2">
							<span class="small opacity-6 text-uppercase">Kategori</span>
						</div>
						<div class="form-group d-md-none">
							<select class="form-control custom-select">
								<option value="0">Semua Video</option>
								<option value="1">Biaya</option>
								<option value="2">Kas & Bank</option>
								<option value="3">Pembelian</option>
								<option value="4">Penjualan</option>
								<option value="5">Pengaturan</option>
							</select>
						</div>
						<div class="d-none d-md-block">
							<span class="btn btn-clear btn-block rounded-2 px-3 text-left" id="category-0">
								Semua Video
							</span>
							<span class="btn btn-clear btn-block rounded-2 px-3 text-left" id="category-1">
								Biaya
							</span>
							<span class="btn btn-clear btn-block rounded-2 px-3 text-left" id="category-2">
								Kas & Bank
							</span>
							<span class="btn btn-clear btn-block rounded-2 px-3 text-left" id="category-3">
								Pembelian
							</span>
							<span class="btn btn-clear btn-block rounded-2 px-3 text-left" id="category-4">
								Penjualan
							</span>
							<span class="btn btn-clear btn-block rounded-2 px-3 text-left" id="category-5">
								Pengaturan
							</span>
						</div>
					</form>
				</div>
				<div class="col-md-8 col-lg-9">
					<div class="row list-video">

						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-youtube-id="1bEDPfhZKOA" data-youtube-duration="01:40">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Apakah Itu Kledo?
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Lihat Selengkapnya di video ini!
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>

						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Kas & Bank" data-youtube-playlist="PLoKPSI6LG_CKKj0D6iWMhKvzMJP4KkVlh" data-youtube-id="zx3ahuqMqVM" data-youtube-duration="03:57" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mencatat-transaksi-kas.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mencatat Transaksi Kas & Bank
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mencatat Transaksi Kas & Bank pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Kas & Bank" data-youtube-playlist="PLoKPSI6LG_CKKj0D6iWMhKvzMJP4KkVlh" data-youtube-id="4ThghprQ3M8" data-youtube-duration="07:36" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-rekonsiliasi-kas-bank.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Rekonsiliasi Kas & Bank
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Rekonsiliasi Kas & Bank pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>

						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Biaya" data-youtube-playlist="PLoKPSI6LG_CLhin9fRb_KqEEPoetKv1tF" data-youtube-id="0BhkHZ00EVo" data-youtube-duration="02:37" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mencatatkan-biaya-dengan-pembayaran-di-belakang.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mencatatkan Biaya dengan Pembayaran Dibelakang
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mencatatkan Biaya dengan Pembayaran Dibelakang pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Biaya" data-youtube-playlist="PLoKPSI6LG_CLhin9fRb_KqEEPoetKv1tF" data-youtube-id="96b2mWznEGs" data-youtube-duration="02:35" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mencatatkan-pengeluaran-biaya.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mencatatkan Pengeluaran Biaya
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mencatatkan Pengeluaran Biaya pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Biaya" data-youtube-playlist="PLoKPSI6LG_CLhin9fRb_KqEEPoetKv1tF" data-youtube-id="R1YCPz5a8S4" data-youtube-duration="03:27" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-import-biaya.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Import Biaya
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Import Biaya pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>

						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pengaturan" data-youtube-playlist="PLoKPSI6LG_CKJ_Q4VpKOsZKLncVTxDi8v" data-youtube-id="G3leI9YL0So" data-youtube-duration="02:55" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mengunci-data.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Pengaturan Tanggal Penguncian
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Tutup Buku atau Tanggal Penguncian pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pengaturan" data-youtube-playlist="PLoKPSI6LG_CKJ_Q4VpKOsZKLncVTxDi8v" data-youtube-id="_tpd3K2eYY8" data-youtube-duration="05:05" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/mengatur-hak-akses-pengguna.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Pengaturan Hak Akses Pengguna Kledo
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mengatur Hak Akses Pengguna Kledo pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>

						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pembelian" data-youtube-playlist="PLoKPSI6LG_CJNukaaUB5jV5fsfFu_HyHy" data-youtube-id="Ktzd6uMlR84" data-youtube-duration="03:41" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/alur-bisnis-pembelian.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Alur Bisnis Pembelian
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai detail alur purchasing atau pembelian dalam software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pembelian" data-youtube-playlist="PLoKPSI6LG_CJNukaaUB5jV5fsfFu_HyHy" data-youtube-id="Re8XwJXyf9c" data-youtube-duration="02:45" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-membuat-retur-pembelian.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Membuat Retur Pembelian
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara mencatat retur pembelian pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pembelian" data-youtube-playlist="PLoKPSI6LG_CJNukaaUB5jV5fsfFu_HyHy" data-youtube-id="Q2MjmBfnt_8" data-youtube-duration="04:54" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-catat-transaksi-pembelian-dan-pembayaran.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mencatat Transaksi Pembelian dan Mengirimkan Pembayaran
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara mencatat Transaksi Pembelian dan Mengirimkan Pembayaran pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pembelian" data-youtube-playlist="PLoKPSI6LG_CJNukaaUB5jV5fsfFu_HyHy" data-youtube-id="Q2MjmBfnt_8" data-youtube-duration="03:40" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-import-data-pembelian.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Import Data Pembelian
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai Cara Import Data Pembelian pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pembelian" data-youtube-playlist="PLoKPSI6LG_CJNukaaUB5jV5fsfFu_HyHy" data-youtube-id="Gy38KgV5Vh0" data-youtube-duration="02:06" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mengubah-dan-hapus-data-pembelian.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mengubah dan Menghapus Transaksi Pembelian
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai Mengubah dan Menghapus Transaksi Pembelian pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Pembelian" data-youtube-playlist="PLoKPSI6LG_CJNukaaUB5jV5fsfFu_HyHy" data-youtube-id="1ee4ImcGvac" data-youtube-duration="03:40" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-membuat-purchase-order.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Membuat Purchase Order
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Membuat Purchase Order pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>

						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="HYfyKMb5LF8" data-youtube-duration="03:04" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-retur-penjualan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Retur Penjualan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai Cara Retur Penjualan pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="oLlFaNl0j5M" data-youtube-duration="02:05" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mengubah-dan-menghapus-transaksi-penjualan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mengubah dan Menghapus Transaksi Penjualan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mengubah dan Menghapus Transaksi Penjualan pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="DyZRL4Ve-zQ" data-youtube-duration="03:42" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/import-data-penjualan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Import Data Penjualan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Import Data Penjualan pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="fGysDQRSVL4" data-youtube-duration="03:47" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/import-status-tagihan-penjualan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Import Status Tagihan Penjualan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Import Status Tagihan Penjualan pada software akuntansi Kledo. Dengan import status tagihan penjualan, ratusan status tagihan dapat di update sekaligus , tidak perlu lagi update status tagihan satu per satu.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="wVXDAghya8I" data-youtube-duration="05:05" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/alur-penjualan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Alur Bisnis Penjualan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai Alur Bisnis Penjualan pada software akuntansi Kledo. Mengelola penjualan dalam bisnis bisa sangat merepotkan. Mulai dari penawaran barang, pengaturan stok digudang, hingga monitor pembayaran jatuh tempo. Namun dengan kehadiran kledo, seluruh alur penjualan itu dapat dikelola dengan mudah dan rapi.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="rZHBXz9GxIM" data-youtube-duration="02:41" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-membuat-surat-jalan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Membuat Surat Jalan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Membuat Surat Jalan pada software akuntansi Kledo. Membuat surat jalan di Kledo tak perlu repot lagi, hanya satu klik. Surat jalan juga otomatis terintegrasi dengan pemesanan, tagihan dan stok gudang lho.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="Gks14j29CUU" data-youtube-duration="02:02" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-cetak-tagihan-per-termin.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mencetak Tagihan Per Termin
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mencetak Tagihan Per Termin pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="5XytTtjPu3E" data-youtube-duration="03:55" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-catat-transaksi-penjualan-menerima-pembayaran.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Mencatat Transaksi Penjualan dan Menerima Pembayaran
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mencatat Transaksi Penjualan dan Menerima Pembayaran pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="JYaxV9nlyKI" data-youtube-duration="03:24" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-membuat-tagihan-berulang.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Membuat Tagihan Berulang
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Membuat Tagihan Berulang pada software akuntansi Kledo. Fitur tagihan berulang memudahkan transaksi bisnis langganan yang mempunyai tagihan setiap bulannya, sehingga tidak perlu membuat tagihan di tiap bulannya jika sudah mengaktifkan fitur ini.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="csfLPCOq8rY" data-youtube-duration="04:22" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mengubah-tampilan-print-invoice.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mengubah Tampilan Print Invoice
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mengubah Tampilan Print Invoice pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="SqY937ISbEM" data-youtube-duration="02:04" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-mencatat-pembayaran-dengan-cicilan.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Mencatat Pembayaran Tagihan dengan Cicilan
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Mencatat Pembayaran Tagihan dengan Cicilan pada software akuntansi Kledo.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>
						<div class="col-lg-6 mb-4 pb-3">
							<div class="card bg-white border-0 rounded-3 shadow text-break h-100" data-category="Penjualan" data-youtube-playlist="PLoKPSI6LG_CLlmNS6UVW0fAacbTJMXtG2" data-youtube-id="ibVBber46G8" data-youtube-duration="01:43" data-youtube-thumbnail="https://kledo.com/wp-content/uploads/2021/04/cara-print-faktur-pajak.jpg">
								<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="#">
									<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
											<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
											<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
										</svg>
									</div>
									<div class="position-absolute right bottom m-2">
										<span class="badge badge-dark font-weight-normal"></span>
									</div>
									<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="#" alt="Thumbnail">
								</div>
								<div class="card-body py-4 px-lg-4">
									<div class="dropdown">
										<button class="btn btn-clear btn-block dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
											<div class="ellipsis line-3">
												Tutorial Cara Print Faktur Pajak
											</div>
										</button>
										<div class="dropdown-menu bg-white shadow my-2">
											<div class="px-3 py-3 py-md-2 mx-1 small opacity-6">
												Pada video ini, akan dijelaskan mengenai cara Print Faktur Pajak pada software akuntansi Kledo. Print faktur pajak dengan mudah di kledo, sebelumnya pastikan transaksi yang akan di print faktur pajaknya memiliki opsi pajak yang aktif di dalamnya.
											</div>
										</div>
									</div>
								</div>
								<div class="card-footer bg-transparent border-0 rounded-3 rounded-top-0 pt-0 pb-4 px-lg-4">
									<div class="d-inline-block small opacity-5 mr-2">
										Bagikan
									</div>
									<a class="text-decoration-none d-inline-block mr-3 share-facebook" rel="noopener" target="_blank" href="https://www.facebook.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M7.8,2h8.4C19.4,2,22,4.6,22,7.8v8.4c0,3.2-2.6,5.8-5.8,5.8H7.8C4.6,22,2,19.4,2,16.2V7.8C2,4.6,4.6,2,7.8,2 M7.6,4C5.6,4,4,5.6,4,7.6l0,0v8.8c0,2,1.6,3.6,3.6,3.6h8.8c2,0,3.6-1.6,3.6-3.6c0,0,0,0,0,0V7.6c0-2-1.6-3.6-3.6-3.6H7.6 M11.7,20v-5.7H9.7V12h2.1v-1.8c0-2,1.2-3.1,3.1-3.1c0.9,0,1.8,0.2,1.8,0.2v2h-1c-1,0-1.3,0.6-1.3,1.3V12h2.2l-0.4,2.3h-1.9V20"/>
										</svg>
										<span class="sr-only">Facebook</span>
									</a>
									<a class="text-decoration-none d-inline-block mr-3 share-twitter" rel="noopener" target="_blank" href="https://twitter.com/">
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="24" height="24" fill="currentColor">
											<path d="M22.46,6C21.69,6.35 20.86,6.58 20,6.69C20.88,6.16 21.56,5.32 21.88,4.31C21.05,4.81 20.13,5.16 19.16,5.36C18.37,4.5 17.26,4 16,4C13.65,4 11.73,5.92 11.73,8.29C11.73,8.63 11.77,8.96 11.84,9.27C8.28,9.09 5.11,7.38 3,4.79C2.63,5.42 2.42,6.16 2.42,6.94C2.42,8.43 3.17,9.75 4.33,10.5C3.62,10.5 2.96,10.3 2.38,10C2.38,10 2.38,10 2.38,10.03C2.38,12.11 3.86,13.85 5.82,14.24C5.46,14.34 5.08,14.39 4.69,14.39C4.42,14.39 4.15,14.36 3.89,14.31C4.43,16 6,17.26 7.89,17.29C6.43,18.45 4.58,19.13 2.56,19.13C2.22,19.13 1.88,19.11 1.54,19.07C3.44,20.29 5.7,21 8.12,21C16,21 20.33,14.46 20.33,8.79C20.33,8.6 20.33,8.42 20.32,8.23C21.16,7.63 21.88,6.87 22.46,6Z"/>
										</svg>
										<span class="sr-only">Twitter</span>
									</a>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<style>
			.list-video .dropdown-toggle {
				text-align: left;
				word-break: break-word;
				padding: 0;
			}
			.list-video .dropdown-toggle:after {
				margin-top: 4px;
			}
		</style>
		<script>
			function embedYoutube(parent,youtube,child,embed) {
				let playlist = youtube + '-playlist'
					id = youtube + '-id'
					duration = youtube + '-duration'
					thumbnail = youtube + '-thumbnail'
				document.querySelectorAll(parent).forEach(item => {
					if ( item.getAttribute(playlist) !== null ) {
						let checkData = setInterval(() => {
							if ( item.querySelector(child).getAttribute('data-detail') !== null ) {
								let detail = item.querySelector(child).getAttribute('data-detail')
									detail = detail.split('?')
									detail = detail[0] + '?' + detail[1] + '&' + detail[2]
								item.querySelector(child).setAttribute('data-detail',detail)
								clearInterval(checkData)
							}
						},100)
						item.querySelector(child).setAttribute(embed,'https://www.youtube.com/embed/' + item.getAttribute(id) + '?list=' + item.getAttribute(playlist))
					} else {
						item.querySelector(child).setAttribute(embed,'https://www.youtube.com/embed/' + item.getAttribute(id))
					}
					if ( item.getAttribute(thumbnail) !== null ) {
						item.querySelector(child).querySelector('img').src = item.getAttribute(thumbnail)
					} else {
						item.querySelector(child).querySelector('img').src = 'https://img.youtube.com/vi/' + item.getAttribute(id) + '/hqdefault.jpg'
					}
					item.querySelector(child).querySelector('.badge').innerHTML = item.getAttribute(duration)
				})
			}
			embedYoutube('.card','data-youtube','.card-header','data-embed')

			function shareYoutube(parent,attribute,facebook,twitter) {
				document.querySelectorAll(parent).forEach(item => {
					let url = item.getAttribute(attribute)
						fb = 'https://facebook.com/sharer.php?display=page&u=https://youtu.be/'
						tw = 'https://twitter.com/intent/tweet?url=https://youtu.be/'
					item.querySelector(facebook).href = fb + url
					item.querySelector(twitter).href = tw + url
				})
			}
			shareYoutube('.card','data-youtube-id','.share-facebook','.share-twitter')

			document.addEventListener('DOMContentLoaded', function() {
				function findItem(form,count,list) {
					$(form + ' input').on('keyup', function() {
						let value = $(this).val()
							keywords = new RegExp(value,'i')

						$(list + ' > *').each(function() {
							let item = $(this)
								keyword = item.find('.card-body').text()
							if (!keyword.search(keywords) >= 0) {
								item.hide()
							}
							if (keyword.search(keywords) >= 0) {
								item.show()
							}
						})

						if ( value.length > 0 ) {
							$(count).show()
							$(form + ' form [type="reset"]').show()
						} else {
							$(count).hide()
							$(form + ' form [type="reset"]').hide()
						}

						$(count + ' span').html($(list + ' > *:visible').length)
						if ( $('#category-0.active').length < 1 ) {
							$('#category-0').click()
						}
					})

					$(form + ' form').on('reset',function(e) {
						$(e.target).find('[type="reset"]').hide()
						$(list + ' > *').show()
						$(count).hide()
					})

					$(form + ' form').on('submit',function(e) {
						e.preventDefault()
					})
				}
				findItem('#findTutorial','#resultTutorial','.list-video')

				function filterItem(parent,list,card) {
					$(parent + ' .btn-clear:first-child').addClass('active')

					$(parent + ' .custom-select').on('change', function() {
						let selectVal = this.value

						$(parent + ' .btn-clear').each(function() {
							let link = $(this)
								linkVal = link.attr('id')
								linkVal = linkVal.split('-')

							link.removeClass('active')
							if ( selectVal == linkVal[1] ) {
								link.addClass('active')
								link.click()
							}
						})
					})

					$(parent + ' .btn-clear').click(function(e) {
						let link = $(this)
							linkVal = link.attr('id')
							linkVal = linkVal.split('-')

						$(parent + ' .custom-select').val(linkVal[1])
						$(parent + ' .btn-clear').removeClass('active')
						link.addClass('active')

						if ( $(window).width() >= 768 ) {
							$('html, body').animate({
								scrollTop: $('#findTutorial').offset().top - 100
							},500)
						}

						$(list + ' ' + card).addClass('d-none')
						$(list + ' ' + card + ' > *').each(function() {
							let item = $(this)
							if ( $.trim(link.text()) == item.data('category') ) {
								item.closest(card).removeClass('d-none')
							}
							if ( $.trim(link.text()) == $(parent + ' .custom-select option:first-child').text() ) {
								item.closest(card).removeClass('d-none')
							}
						})

						if ( $('#category-0.active').length < 1 ) {
							$('#findTutorial [type="reset"]').click()
						}
					})
				}
				filterItem('.pills-responsive','.list-video','.col-lg-6')
			})
		</script>
	</article>

	<article id="popupModal">
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				popupYoutube('#popupModal','#popupYoutube',800,450)
			})
		</script>
	</article>

	<article>
		<div class="container py-5 position-relative text-center">
			<div class="row">
				<div class="col-12">
					<h2 class="fs-2 text-break mb-3">
						Ada pertanyaan?
					</h2>
					<p class="fs-6 opacity-6 mb-3 pb-1">
						Silakan hubungi kami melalui WhatsApp
					</p>
					<a class="btn btn-success rounded-pill py-2 pl-3 pr-4 lh-1 fs-6 font-title text-uppercase ws-nowrap" href="https://wa.me/6282383334000&text=Hallo, Saya mau tanya tentang Kledo.">
						<div class="d-flex align-items-center">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="currentColor" class="m-1">
								<path d="M12.04 2C6.58 2 2.13 6.45 2.13 11.91C2.13 13.66 2.59 15.36 3.45 16.86L2.05 22L7.3 20.62C8.75 21.41 10.38 21.83 12.04 21.83C17.5 21.83 21.95 17.38 21.95 11.92C21.95 9.27 20.92 6.78 19.05 4.91C17.18 3.03 14.69 2 12.04 2M12.05 3.67C14.25 3.67 16.31 4.53 17.87 6.09C19.42 7.65 20.28 9.72 20.28 11.92C20.28 16.46 16.58 20.15 12.04 20.15C10.56 20.15 9.11 19.76 7.85 19L7.55 18.83L4.43 19.65L5.26 16.61L5.06 16.29C4.24 15 3.8 13.47 3.8 11.91C3.81 7.37 7.5 3.67 12.05 3.67M8.53 7.33C8.37 7.33 8.1 7.39 7.87 7.64C7.65 7.89 7 8.5 7 9.71C7 10.93 7.89 12.1 8 12.27C8.14 12.44 9.76 14.94 12.25 16C12.84 16.27 13.3 16.42 13.66 16.53C14.25 16.72 14.79 16.69 15.22 16.63C15.7 16.56 16.68 16.03 16.89 15.45C17.1 14.87 17.1 14.38 17.04 14.27C16.97 14.17 16.81 14.11 16.56 14C16.31 13.86 15.09 13.26 14.87 13.18C14.64 13.1 14.5 13.06 14.31 13.3C14.15 13.55 13.67 14.11 13.53 14.27C13.38 14.44 13.24 14.46 13 14.34C12.74 14.21 11.94 13.95 11 13.11C10.26 12.45 9.77 11.64 9.62 11.39C9.5 11.15 9.61 11 9.73 10.89C9.84 10.78 10 10.6 10.1 10.45C10.23 10.31 10.27 10.2 10.35 10.04C10.43 9.87 10.39 9.73 10.33 9.61C10.27 9.5 9.77 8.26 9.56 7.77C9.36 7.29 9.16 7.35 9 7.34C8.86 7.34 8.7 7.33 8.53 7.33Z"/>
							</svg>
							<span class="mx-1">
								0823 8333 4000
							</span>
						</div>
					</a>
				</div>
			</div>
			<img class="obj-fit-cover obj-position-top position-absolute right bottom mb-n5 mr-5 d-none d-md-block" loading="lazy" width="275" height="300" src="upload/woman.png">
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

<?php get_footer() ?>