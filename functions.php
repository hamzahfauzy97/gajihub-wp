<?php

add_action('get_header', 'my_filter_head');

function my_filter_head() {
    remove_action('wp_head', '_admin_bar_bump_cb');
}

add_theme_support( 'title-tag' );
// This theme uses post thumbnails
add_theme_support( 'post-thumbnails' );

function bootstrapstarter_register_menu() {
    register_nav_menu('header-menu', __( 'Header Menu' ));
}
add_action( 'init', 'bootstrapstarter_register_menu' );

function bootstrapstarter_register_footer_menu() {
    register_nav_menu('footer-menu', __( 'Footer Menu' ));
}
add_action( 'init', 'bootstrapstarter_register_footer_menu' );

// add_action('after_setup_theme', 'mytheme_setup');

function mytheme_setup(){

//  if(get_option('page_on_front')=='0' && get_option('show_on_front')=='posts'){
    $pages = [
        'Tentang'        => ['template/about-template.php','about'],
        'Karir'          => ['template/career-template.php','career'],
        'Demo'           => ['template/demo-template.php','demo'],
        'Fitur'          => ['template/feature-template.php','feature'],
        'Harga'          => ['template/price-template.php','price'],
        'Privasi'        => ['template/privacy-template.php','privacy'],
        'Proposal'       => ['template/proposal-template.php','proposal'],
        'Term'           => ['template/term-template.php','term'],
        'Tutorial'       => ['template/tutorial-template.php','tutorial'],
        'Tutorial Video' => ['template/tutorial-video-template.php','video'],
    ];

    foreach($pages as $page => $template)
    {
        // Create homepage
        $_page = array(
            'post_type'     => 'page',
            'post_name'     => $template[1],
            'post_title'    => $page,
            'post_content'  => '',
            'post_status'   => 'publish',
            'post_author'   => 1
        ); 
        // Insert the post into the database
        $_page_id =  wp_insert_post( $_page );
        //set the page template 
        //assuming you have defined template on your-template-filename.php
        update_post_meta($_page_id, '_wp_page_template', $template[0]);
    }
    // }

}

if(isset($_POST['install_dummy_page']) && get_page_by_title('Tentang') == null)
{
    mytheme_setup();
    // do_settings_sections(__FILE__);
}

// wp theme setting
function build_options_page() { ?>
    <div id="theme-options-wrap">
        <div class="icon32" id="icon-tools"> <br /> </div>
        <h2>Theme Settings</h2>
        <form method="post" action="options.php" enctype="multipart/form-data">
            <input type="hidden" name="install_dummy_page" value="true">
            <?php settings_fields('theme_options'); ?>
            <?php // do_settings_sections(__FILE__); ?>
            <p class="submit">
                <input name="Submit" type="submit" class="button-primary" value="<?php esc_attr_e('Install Dummy Page'); ?>" />
            </p>
        </form>
    </div>
<?php }
add_action('admin_init', 'register_and_build_fields');
function register_and_build_fields() {
    register_setting('theme_options', 'theme_options', 'validate_setting');
    add_settings_section('homepage_settings', 'Homepage Settings', 'section_homepage', __FILE__);
    function section_homepage() {}
    add_settings_field('logooption', 'Logo Option', 'logo_option', __FILE__, 'homepage_settings');
    add_settings_field('logotext', 'Logo Text', 'logo_text', __FILE__, 'homepage_settings');
    add_settings_field('logoimage', 'Logo Image URL (300px x 60px)', 'logo_image', __FILE__, 'homepage_settings');
}
    
function validate_setting($theme_options) {
    return $theme_options;
}

function logo_option()
{
    $options = get_option('theme_options');  
    echo "<select name='theme_options[logo_type]'><option value='text' ".($options['logo_type']=='text' ? 'selected' : '').">Text</option><option value='image' ".($options['logo_type']=='image' ? 'selected' : '').">Image</option></select>";
}

function logo_text()
{
    $options = get_option('theme_options');  
    echo "<input name='theme_options[logo_text]' type='text' value='{$options['logo_text']}' />";
}

function logo_image()
{
    $options = get_option('theme_options');  
    echo "<input name='theme_options[logo_image]' type='text' value='{$options['logo_image']}' />";
}

add_action('admin_menu', 'theme_options_page');
function theme_options_page() {  
    add_options_page('Theme Settings', 'Theme Settings', 'administrator', __FILE__, 'build_options_page');
}

