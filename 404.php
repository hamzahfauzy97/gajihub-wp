<?php get_header() ?>
	<article id="header">
		<div class="container pt-5 pb-5 pb-md-3">
			<div class="row">
				<div class="col-12 col-md-11 col-lg-10 col-xl-9 mx-auto">
					<h1 class="fs-1 text-break text-center mb-0">
						404. Page Not Found!
					</h1>
				</div>
			</div>
		</div>
	</article>
<?php get_footer() ?>