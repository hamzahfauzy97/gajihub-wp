<?php get_header() ?>

    <article id="header" class="bg-theme-1 text-white pb-md-4 pb-lg-5 pt-lg-3">
		<div class="container py-5">
			<div class="row no-gutters align-items-lg-center">
				<div class="col-lg-6 order-lg-2 text-center">
					<img class="img-fluid px-5 mb-4 mb-lg-0" width="600" height="400" src="<?= get_template_directory_uri() ?>/upload/slider-1.png" alt="GajiHub App">
					<!-- <div id="carousel-main" class="carousel slide mb-4 mb-lg-0" data-ride="carousel" data-pause="false" data-interval="4000">
						<div class="carousel-inner">
							<div class="carousel-item active">
								<img class="img-fluid px-5" width="600" height="400" src="<?= get_template_directory_uri() ?>/upload/slider-1.png" alt="Banner 1">
							</div>
							<div class="carousel-item">
								<img class="img-fluid px-5" width="600" height="400" src="<?= get_template_directory_uri() ?>/upload/slider-2.png" alt="Banner 2">
							</div>
						</div>
					</div>
					<style>
						#carousel-main:before,
						#carousel-main:after {
							content: '';
							position: absolute;
							top: 0;
							width: 3rem;
							height: 100%;
							z-index: 1;
						}
						#carousel-main:before {
							left: 0;
							background-image: linear-gradient(90deg, var(--theme-1) 0%, transparent 100%);
						}
						#carousel-main:after {
							right: 0;
							background-image: linear-gradient(-90deg, var(--theme-1) 0%, transparent 100%);
						}
					</style> -->
				</div>
				<div class="col-lg-6 order-lg-1 text-center text-lg-left">
					<h1 class="fs-1 text-break mb-3">
						Sudahi Kerepotan Pengelolaan HR dan Payroll Bisnis Anda
					</h1>
					<p class="fs-6 text-break my-3 opacity-7">
						Dengan GajiHub, Anda bisa mengelola proses Payroll dan HR dalam satu software terintegrasi. Hitung gaji, pajak, dan pengelolaan data karyawan jadi lebih praktis.
					</p>
					<div class="d-inline-block my-2 ml-lg-n2">
						<a class="btn btn-warning rounded-pill py-2 pl-4 fs-7 text-uppercase ws-nowrap mx-2" href="#">
							<div class="d-flex align-items-center">
								<span class="ml-1 mr-2">
									Coba <b>Gratis 14 Hari</b>
								</span>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40">
									<path fill="#292D32" opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
									<path fill="#fff" d="M16.03 11.4699L13.03 8.46994C12.74 8.17994 12.26 8.17994 11.97 8.46994C11.68 8.75994 11.68 9.23994 11.97 9.52994L13.69 11.2499H8.5C8.09 11.2499 7.75 11.5899 7.75 11.9999C7.75 12.4099 8.09 12.7499 8.5 12.7499H13.69L11.97 14.4699C11.68 14.7599 11.68 15.2399 11.97 15.5299C12.12 15.6799 12.31 15.7499 12.5 15.7499C12.69 15.7499 12.88 15.6799 13.03 15.5299L16.03 12.5299C16.32 12.2399 16.32 11.7599 16.03 11.4699Z"/>
								</svg>
							</div>
						</a>
					</div>
					<div class="d-inline-block my-2 ml-lg-n2 ml-xl-0">
						<button type="button" class="btn btn-outline-light rounded-pill py-2 pl-4 fs-7 text-uppercase ws-nowrap mx-2" data-toggle="modal" data-target="#popupYoutube" data-embed="https://www.youtube.com/embed/1bEDPfhZKOA">
							<div class="d-flex align-items-center">
								<span class="ml-1 mr-2">
									Lihat <b>Video Demo</b>
								</span>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40">
									<path fill="#292D32" opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
									<path fill="#fff" d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
								</svg>
							</div>
						</button>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article class="position-relative bg-light">
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1)" class="overlay-top overlay-flip-y">
			<polygon points="100,30 0,30 0,25 100,0"/>
		</svg>
		<div class="container py-5">
			<div class="row py-4">
				<div class="col-lg-6 order-lg-2 text-center">
					<img class="img-fluid mb-4" loading="lazy" width="600" height="400" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/backgrounds/hero-2.png" alt="Semua proses manajemen SDM kini dalam satu sistem">
				</div>
				<div class="col-lg-6 order-lg-1 text-center text-lg-left">
					<h2 class="fs-2 text-break mb-4">
						Semua proses manajemen SDM kini dalam satu sistem
					</h2>
					<p class="fs-6 text-break opacity-5">
						Dengan satu platform untuk data karyawan, HR, dan operasional, semua orang dapat mengakses data secara real time untuk membuat keputusan yang tepat.
					</p>
					<p class="fs-6 text-break opacity-5">
						Anda dapat mengandalkan software HR dan payroll GajiHub untuk menghadirkan teknologi yang mendorong pertumbuhan bisnis dan menjaga keamanan data Anda.
					</p>
				</div>
			</div>
		</div>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

	<article>
		<div class="container py-5">
			<div class="row mb-3">
				<div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
					<h2 class="fs-2 text-break mb-3 text-center">
						Bagaimana Software HR dan Payroll GajiHub Membantu Anda?
					</h2>
				</div>
			</div>
			<div class="row text-center hover-card">
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-1.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Kelola data karyawan lebih mudah</h3>
							<p class="opacity-5 text-break">Seluruh data karyawan bisa Anda akses dan kelola dalam satu sistem sehingga memudahkan Anda dalam melakukan manajemen SDM lebih efisien.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-2.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Mencatat absensi karyawan</h3>
							<p class="opacity-5 text-break">Informasi dan pencatatan absensi berjalan lebih optimal dengan dengan integrasi langsung ke sistem penggajian perusahan.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-3.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Mengelola pola kerja</h3>
							<p class="opacity-5 text-break">Perusahaan Anda menerapkan peraturan jam kerja yang beragam atau memiliki pembagian pola kerja? Dengan GajiHub membuat pengelolaan jam kerja menjadi lebih mudah!</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-4.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Pengelolaan cuti dan izin karyawan</h3>
							<p class="opacity-5 text-break">Anda mengelola izin karyawan yang berlaku di perusahaan Anda dengan mudah. Di GajiHub Anda juga fleksibel mengakomodasi periode cuti individu dan periode bersama.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-5.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Analisis kinerja karyawan</h3>
							<p class="opacity-5 text-break">GajiHub menyediakan fitur yang Anda butuhkan untuk Analisis Kinerja Karyawan secara real-time di satu tempat. Mulai dari Data Kompensasi, Demografi Karyawan, sampai Data Produktivitas bisa Anda peroleh di GajiHub.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-6.png" alt="Icont Lainnya">
							<h3 class="fs-4 text-break mb-3">Menghitung gaji, THR, dan benefit Lainnya</h3>
							<p class="opacity-5 text-break">Sistem penggajian karyawan di GajiHub fleksibel dan dapat diakses online memudahkan perhitungan gaji karyawan dengan beragam komponen tunjangan, termasuk mengkalkulasi lembur, THR, bonus dan benefit lainnya.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-7.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Penghitungan pajak karyawan dan BPJS</h3>
							<p class="opacity-5 text-break">Mulai dari penghitungan PPh 21 dan PPh 26, kini Anda bisa melakukan itu semua dengan praktis di GajiHub. Anda juga dapat menghitung premi BPJS, baik BPJS Ketenagakerjaan maupun BPJS langsung dari sistem GajiHub.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-8.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Multi approval</h3>
							<p class="opacity-5 text-break">Pengajuan cuti karyawan membutuhkan persetujuan lebih dari satu orang atau beberapa level yang berbeda? Di GajiHub, proses ini dapat dikelola dengan mudah sesuai struktur organisasi yang berlaku di perusahaan.</p>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-4 mx-auto mb-4 pb-md-3">
					<div class="card h-100 p-2 p-md-3 rounded-2 border-0 shadow hover-1">
						<div class="card-body">
							<img class="mb-3" loading="lazy" width="100" height="100" src="<?= get_template_directory_uri() ?>/upload/icon-9.png" alt="Icon">
							<h3 class="fs-4 text-break mb-3">Struktur dan skala upah</h3>
							<p class="opacity-5 text-break">Dengan GajiHub, Anda bisa mengatur struktur dan skala upah. Dengan adanya fitur ini, Anda bisa merancang struktur golongan jabatan beserta range pendapatan berdasarkan golongan tersebut. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article class="parallax-overlay light mb-3">
		<section class="parallax py-4" data-parallax="<?= get_template_directory_uri() ?>/upload/banner-3.jpeg">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-top mt-n4">
				<polygon points="0,0 0,5 50,30 100,5 100,0"/>
			</svg>
			<div class="container pt-5 pb-3">
				<div class="px-xl-5 mx-xl-3">
					<div class="d-flex justify-content-center font-title mb-5 pb-3 pricing-switch">
						<div class="d-flex bg-white rounded-pill shadow p-2">
							<button type="button" class="btn btn-clear fs-6 position-relative pointer-events-none" onclick="switchPrice('.pricing-switch','.pricing')">
								<span>per <b>Tahun</b></span>
								<div class="position-absolute top left right mt-n4">
									<span class="badge badge-success text-white rounded-pill p-2">5%</span>
								</div>
							</button>
							<div class="custom-control custom-switch custom-switch-lg switch-always-on switch-overlay ml-2" onclick="switchPrice('.pricing-switch','.pricing')">
								<input type="checkbox" class="custom-control-input" id="switchPricing">
								<label class="custom-control-label" for="switchPricing">
									<span class="sr-only">Switch Pricing</span>
								</label>
							</div>
							<button type="button" class="btn btn-clear fs-6" onclick="switchPrice('.pricing-switch','.pricing')">
								<span class="opacity-5">per <b>Bulan</b></span>
							</button>
						</div>
					</div>
					<div class="row no-gutters pricing">
						<div class="col-md-4 mb-4 pb-2 mt-md-4 pt-md-2">
							<div class="card bg-white border-0 rounded-3 rounded-right-md-0 shadow text-center text-break">
								<div class="card-body py-4 px-lg-4">
									<h3 class="fs-5 lh-2 text-uppercase font-title font-weight-bold my-2">
										Gratis
									</h3>
									<div class="font-title mt-3 mb-4">
										<div class="fs-1 d-flex align-items-center justify-content-center text-warning lh-2 mr-4 mr-lg-0 mr-xl-4 pr-2 pr-md-3">
											<span class="fs-4 opacity-2 text-dark mb-1 mr-1">Rp</span>
											<b>0</b>
											<b class="d-none">0</b>
										</div>
										<div class="opacity-3">selamanya</div>
									</div>
									<div class="separator my-3 mx-auto w-75"></div>
									<ul class="list-group list-group-flush lh-2 mb-0">
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0 d-none d-md-block">&nbsp;</li>
									</ul>
								</div>
								<div class="card-footer bg-warning w-25 border-0 rounded-3 py-1 mt-2 mb-4 mx-auto"></div>
							</div>
						</div>
						<div class="col-md-4 mb-4 pb-2 index-1">
							<div class="card bg-white border-0 rounded-3 shadow text-center text-break">
								<div class="card-body py-4 px-lg-4">
									<h3 class="fs-5 lh-2 text-uppercase font-title font-weight-bold my-2">
										Elit
									</h3>
									<div class="font-title mt-3 mb-4">
										<div class="fs-1 d-flex align-items-center justify-content-center text-theme-1 lh-2 mr-4 mr-lg-0 mr-xl-4">
											<span class="fs-4 opacity-2 text-dark mb-1 mr-1">Rp</span>
											<b>5700</b>
											<b class="d-none">6000</b>
										</div>
										<div class="opacity-3">per bulan</div>
									</div>
									<div class="separator my-3 mx-auto w-75"></div>
									<ul class="list-group list-group-flush lh-2 mb-0">
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									</ul>
								</div>
								<div class="card-footer bg-theme-1 w-25 border-0 rounded-3 py-1 mt-2 mb-4 mx-auto"></div>
							</div>
						</div>
						<div class="col-md-4 mb-4 pb-2 mt-md-4 pt-md-2">
							<div class="card bg-white border-0 rounded-3 rounded-left-md-0 shadow text-center text-break">
								<div class="card-body py-4 px-lg-4">
									<h3 class="fs-5 lh-2 text-uppercase font-title font-weight-bold my-2">
										Pro
									</h3>
									<div class="font-title mt-3 mb-4">
										<div class="fs-1 d-flex align-items-center justify-content-center text-info lh-2 mr-4 mr-lg-0 mr-xl-4">
											<span class="fs-4 opacity-2 text-dark mb-1 mr-1">Rp</span>
											<b>4750</b>
											<b class="d-none">5000</b>
										</div>
										<div class="opacity-3">per bulan</div>
									</div>
									<div class="separator my-3 mx-auto w-75"></div>
									<ul class="list-group list-group-flush lh-2 mb-0">
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
										<li class="list-group-item bg-transparent border-0 py-2 px-0">...</li>
									</ul>
								</div>
								<div class="card-footer bg-info w-25 border-0 rounded-3 py-1 mt-2 mb-4 mx-auto"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-bottom mb-n4">
				<polygon points="100,30 0,30 0,0 50,25 100,0"/>
			</svg>
		</section>
	</article>

	<article>
		<div class="container pt-5 pb-3">
			<div class="row text-center">
				<div class="col-md-8 col-lg-7 col-xl-6 mx-auto">
					<h2 class="fs-2 text-break mb-5">
						Mereka Yang Sudah Menggunakan GajiHub
					</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mb-4 pb-2 mt-md-4 pt-md-3">
					<div class="card bg-white border-0 rounded-3 shadow text-center text-break hover-1">
						<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="https://www.youtube.com/embed/n8MYkAllSVA">
							<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
									<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
									<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
								</svg>
							</div>
							<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="https://dev-wp.s3-ap-southeast-1.amazonaws.com/kerjoo.jpg" alt="Author – Jobs">
						</div>
						<div class="card-body py-4 px-lg-4">
							<p class="opacity-7">
								Eu reprehenderit elit id quis irure in ut elit. Irure do ex incididunt fugiat excepteur reprehenderit eiusmod veniam. Lorem ipsum aliquip eiusmod incididunt aute reprehenderit ut ex amet cupidatat ex laboris.
							</p>
							<span class="d-inline-block small bg-theme-1 text-white rounded-2 py-2 px-3">
								<b>Author</b> – Jobs
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-4 pb-2">
					<div class="card bg-white border-0 rounded-3 shadow text-center text-break hover-1">
						<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="https://www.youtube.com/embed/09i9VKDpx8w">
							<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
									<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
									<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
								</svg>
							</div>
							<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="https://dev-wp.s3-ap-southeast-1.amazonaws.com/niffly.JPG" alt="Author – Jobs">
						</div>
						<div class="card-body py-4 px-lg-4">
							<p class="opacity-7">
								Lorem ipsum dolore reprehenderit consequat laborum dolore quis eu velit dolore. Laborum do velit mollit aute do proident ea et. Ut consectetur dolor dolore velit veniam sunt adipisicing nulla duis.
							</p>
							<span class="d-inline-block small bg-theme-1 text-white rounded-2 py-2 px-3">
								<b>Author</b> – Jobs
							</span>
						</div>
					</div>
				</div>
				<div class="col-md-4 mb-4 pb-2 mt-md-4 pt-md-3">
					<div class="card bg-white border-0 rounded-3 shadow text-center text-break hover-1">
						<div class="card-header position-relative border-0 p-0 cursor-pointer" data-toggle="modal" data-target="#popupYoutube" data-embed="https://www.youtube.com/embed/8q6oy5wRTrA">
							<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="40" height="40" fill="#fff">
									<path opacity=".25" d="M12 22C17.5228 22 22 17.5228 22 12C22 6.47715 17.5228 2 12 2C6.47715 2 2 6.47715 2 12C2 17.5228 6.47715 22 12 22Z"/>
									<path d="M9.09961 12V10.52C9.09961 8.60999 10.4496 7.83999 12.0996 8.78999L13.3796 9.52999L14.6596 10.27C16.3096 11.22 16.3096 12.78 14.6596 13.73L13.3796 14.47L12.0996 15.21C10.4496 16.16 9.09961 15.38 9.09961 13.48V12Z"/>
								</svg>
							</div>
							<img class="obj-fit-cover rounded-3 rounded-bottom-0" loading="lazy" width="600" height="400" src="https://dev-wp.s3-ap-southeast-1.amazonaws.com/batiklurik.JPG" alt="Author – Jobs">
						</div>
						<div class="card-body py-4 px-lg-4">
							<p class="opacity-7">
								Mollit incididunt magna velit sed aute laboris irure non est consectetur dolore officia veniam irure. Sit ex nostrud esse voluptate anim sunt ut consequat labore cupidatat incididunt dolor dolore. Esse ad ex qui tempor cupidatat sit quis.
							</p>
							<span class="d-inline-block small bg-theme-1 text-white rounded-2 py-2 px-3">
								<b>Author</b> – Jobs
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>

	<article class="position-relative bg-theme-1">
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--white)" class="overlay-top">
			<polygon points="0,0 0,5 50,30 100,5 100,0"/>
		</svg>
		<section class="overflow-hidden text-white">
			<div class="container py-5">
				<div class="row mb-3 text-center">
					<div class="col-md-8 col-lg-7 col-xl-6 mx-auto">
						<h2 class="fs-2 text-break mb-3">
							Telah Diliput Oleh
						</h2>
					</div>
				</div>
				<div class="mb-3">
					<div id="carousel-client" class="carousel slide carousel-multi" data-ride="carousel" data-pause="false" data-interval="4000">
						<div class="carousel-inner row w-100 mx-auto">
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/kontan.png" alt="Kontan">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/wartaekonomi.png" alt="Warta Ekonomi">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/infokomputer.png" alt="InfoKomputer">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/investing.png" alt="Investing">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/tribun.png" alt="Tribun">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/tempoco.png" alt="Tempo">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/tagar.png" alt="Tagar">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/itech.png" alt="iTech">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/kedaulatan-rakyat.png" alt="Kedaulatan Rakyat">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/siar.png" alt="SIAR">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/kabarbisnis.png" alt="Kabar Bisnis">
								</div>
							</div>
							<div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3">
								<div class="bg-white text-center shadow rounded-2 py-3 px-4 mx-auto hover-1">
									<img class="img-fluid d-block" loading="lazy" width="128" height="37" src="https://kledo.com/wp-content/themes/new-theme/assets2/img/media/linenews.png" alt="Line News">
								</div>
							</div>
						</div>
						<a class="carousel-control-prev" href="#carousel-client" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carousel-client" role="button" data-slide="next">
							<span class="carousel-control-next-icon" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
					</div>
					<style>
						.carousel-multi .carousel-control-prev,
						.carousel-multi .carousel-control-next {
							filter: invert(1);
						}
						.carousel-multi .carousel-inner {
							overflow: initial;
						}
						.carousel-multi .carousel-item > div,
						.carousel-multi .carousel-control-prev,
						.carousel-multi .carousel-control-next {
							width: -moz-fit-content;
							width: fit-content;
						}
						@media (max-width: 768px) {
							.carousel-multi:not(:hover) .carousel-control-next-icon,
							.carousel-multi:not(:hover) .carousel-control-prev-icon {
								opacity: 1;
							}
						}
						/*
							code by Iatek LLC 2018 - CC 2.0 License - Attribution required
							code customized by Azmind.com
						*/
						@media (min-width: 768px) and (max-width: 991px) {
							.carousel-multi .carousel-inner .active.col-md-4.carousel-item + .carousel-item + .carousel-item + .carousel-item {
								position: absolute;
								top: 0;
								right: -33.3333%;
								z-index: -1;
								display: block;
								visibility: visible;
							}
						}
						@media (min-width: 576px) and (max-width: 768px) {
							.carousel-multi .carousel-inner .active.col-sm-6.carousel-item + .carousel-item + .carousel-item {
								position: absolute;
								top: 0;
								right: -50%;
								z-index: -1;
								display: block;
								visibility: visible;
							}
						}
						@media (min-width: 576px) {
							.carousel-multi .carousel-item {
								margin-right: 0;
							}
							.carousel-multi .carousel-inner .active + .carousel-item {
								display: block;
							}
							.carousel-multi .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
							.carousel-multi .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item {
								transition: none;
							}
							.carousel-multi .carousel-inner .carousel-item-next {
								position: relative;
								transform: translate3d(0, 0, 0);
							}
							.carousel-multi .active.carousel-item-left + .carousel-item-next.carousel-item-left,
							.carousel-multi .carousel-item-next.carousel-item-left + .carousel-item,
							.carousel-multi .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item {
								position: relative;
								transform: translate3d(-100%, 0, 0);
								visibility: visible;
							}
							.carousel-multi .carousel-inner .carousel-item-prev.carousel-item-right {
								position: absolute;
								top: 0;
								left: 0;
								z-index: -1;
								display: block;
								visibility: visible;
							}
							.carousel-multi .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
							.carousel-multi .carousel-item-prev.carousel-item-right + .carousel-item,
							.carousel-multi .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item {
								position: relative;
								transform: translate3d(100%, 0, 0);
								visibility: visible;
								display: block;
								visibility: visible;
							}
						}
						@media (min-width: 768px) {
							.carousel-multi .carousel-inner .active + .carousel-item + .carousel-item {
								display: block;
							}
							.carousel-multi .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item {
								transition: none;
							}
							.carousel-multi .carousel-inner .carousel-item-next {
								position: relative;
								transform: translate3d(0, 0, 0);
							}
							.carousel-multi .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item {
								position: relative;
								transform: translate3d(-100%, 0, 0);
								visibility: visible;
							}
							.carousel-multi .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item {
								position: relative;
								transform: translate3d(100%, 0, 0);
								visibility: visible;
								display: block;
								visibility: visible;
							}
						}
						@media (min-width: 991px) {
							.carousel-multi .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item {
								display: block;
							}
							.carousel-multi .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
								transition: none;
							}
							.carousel-multi .carousel-inner .active.col-lg-3.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
								position: absolute;
								top: 0;
								right: -25%;
								z-index: -1;
								display: block;
								visibility: visible;
							}
							.carousel-multi .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
								position: relative;
								transform: translate3d(-100%, 0, 0);
								visibility: visible;
							}
							.carousel-multi .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
								position: relative;
								transform: translate3d(100%, 0, 0);
								visibility: visible;
								display: block;
								visibility: visible;
							}
						}
					</style>
					<script>
						document.addEventListener('DOMContentLoaded', function() {
							$('#carousel-client').on('slide.bs.carousel', function(e) {
								var $e = $(e.relatedTarget);
								var idx = $e.index();
								var itemsPerSlide = 5;
								var totalItems = $('.carousel-multi .carousel-item').length;
								if (idx >= totalItems-(itemsPerSlide-1)) {
									var it = itemsPerSlide - (totalItems - idx);
									for (var i=0; i<it; i++) {
										if (e.direction=="left") {
											$('.carousel-multi .carousel-item').eq(i).appendTo('.carousel-multi .carousel-inner');
										}
										else {
											$('.carousel-multi .carousel-item').eq(0).appendTo('.carousel-multi .carousel-inner');
										}
									}
								}
							})
						})
					</script>
				</div>
			</div>
		</section>
		<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" viewBox="0 0 100 30" width="100%" height="50" fill="var(--theme-1-hover)" class="overlay-bottom">
			<polygon points="0,0 100,25 100,30 0,30"/>
		</svg>
	</article>

	<article id="popupModal">
		<script>
			document.addEventListener('DOMContentLoaded', function() {
				popupYoutube('#popupModal','#popupYoutube',800,450)
			})
		</script>
	</article>

<?php get_footer() ?>