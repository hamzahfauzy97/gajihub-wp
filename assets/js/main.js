/*
 * GajiHub
 * https://gajihub.com
 * Designed by Degiam [https://degiam.github.io]
 * Copyright (c) 2021
 */

// User Scalable
	document.querySelectorAll('[name="viewport"]').forEach(viewport => {
		var standalone = window.navigator.standalone
			userAgent = window.navigator.userAgent.toLowerCase()
			safari = /safari/.test(userAgent)
			ios = /iphone|ipod|ipad/.test(userAgent)

		if (ios) {
			if ( !standalone && safari ) {
				viewport.content = 'width=device-width, minimum-scale=1, initial-scale=1, maximum-scale=1, user-scalable=0'
			} else if ( standalone && !safari ) {
				viewport.content = 'width=device-width, minimum-scale=1, initial-scale=1, maximum-scale=1, user-scalable=0'
			} else if ( !standalone && !safari ) {
				viewport.content = 'width=device-width, minimum-scale=1, initial-scale=1, maximum-scale=1, user-scalable=0'
			}
		}
	})

// Navbar
	$('.dropdown-menu .dropdown-toggle').on('click', function(e) {
		let toggle = $(this)

		if (!toggle.next().hasClass('show')) {
			toggle.parents('.dropdown-menu').first().find('.show').removeClass('show')
			toggle.parents('.dropdown-menu').first().find('.open').removeClass('open')
		}

		toggle.toggleClass('open')
		toggle.next('.dropdown-menu').toggleClass('show')

		toggle.parents('.dropdown.show').on('hidden.bs.dropdown', function(e) {
			$('.dropdown-submenu .show').removeClass('show')
			$('.dropdown-submenu .open').removeClass('open')
		})

		return false
	})

	function navMove() {
		if ( document.body.clientWidth <= 767 ) {
			$('#navbarButton .nav-move').each(function() {
				$(this).appendTo($('#navbarMenu .navbar-nav'))
			})
		} else {
			$('#navbarMenu .nav-move').each(function() {
				$(this).appendTo($('#navbarButton'))
			})
			$('.menu-opened').removeClass('menu-opened')
		}
	}
	navMove()
	$(window).resize(navMove)

	function navbarScroll(target,add,y) {
		let element = add.substring(1)
		if ( $(target + add).length < 1 ) {
			if ( $(window).scrollTop() >= y ) {
				$(target).addClass(element)
			}
			$(window).scroll(function() {
				if ( $(window).scrollTop() >= y ) {
					$(target).addClass(element)
				} else {
					$(target).removeClass(element)
				}
			})
		} else {
			$(window).scroll(function() {
				if ( $(window).scrollTop() <= (y - 1) ) {
					$(target).removeClass(element)
				}
			})
		}
	}

// Parallax
	function parallaxImg() {
		let slot = document.querySelector('slot')
			viewport = document.querySelector('body').clientHeight
			parallax = document.createElement('style')
			parallax.innerHTML = `.parallax{position:relative}.parallax:after,.parallax:before{content:'';position:absolute;top:0;left:0;right:0;bottom:0;z-index:-1}.parallax:before{background-position:center;background-size:cover;background-repeat:no-repeat;z-index:-2}.parallax-overlay.dark .parallax:after{background-color:rgba(0,0,0,.65)}.parallax-overlay.light .parallax:after{background-color:rgba(255,255,255,.75)}@media (min-width:768px){.parallax:before{background-attachment:fixed;}}`
		slot.appendChild(parallax)

		document.querySelectorAll('.parallax').forEach((item,index) => {
			let prlxcss = document.createElement('style')
				prlxcss.innerHTML = `.parallax-` + index + `:before{background-image:url(` + item.getAttribute(`data-parallax`) + `)}`
			item.classList.add('parallax-' + index)
			slot.appendChild(prlxcss)
		})
	}
	parallaxImg()

// Price
	function switchPrice(parent,change) {
		$(change + ' .fs-1 b').toggleClass('d-none')
		$(parent).find('button').toggleClass('pointer-events-none')
		$(parent).find('button>span').toggleClass('opacity-5')
		let input = $(parent).find('input')
		if ( input.attr('checked') != null) {
			input.removeAttr('checked')
		} else {
			input.attr('checked','checked')
		}
	}
	$('#switchPricing').on('change',function() {
		switchPrice('.pricing-switch','.pricing')
		let input = $(this)
		if ( input.attr('checked') != null) {
			input.removeAttr('checked')
		} else {
			input.attr('checked','checked')
		}
	})

// Popup Youtube
	function popupYoutube(parent,selector,width,height) {
		$(`<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="` + selector.substring(1) + `">
			<button type="button" class="btn btn-clear position-absolute top right m-1 p-2 text-white modal-close" data-dismiss="modal" aria-label="Close">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="30" height="30" fill="currentColor">
					<path d="M17.7,18.8L5.2,6.3C5,6,5,5.5,5.2,5.2C5.5,5,6,5,6.3,5.2l12.4,12.4c0.3,0.3,0.3,0.8,0,1.1C18.5,19,18,19,17.7,18.8z"/>
					<path d="M5.2,17.7L17.7,5.2C18,5,18.5,5,18.8,5.2C19,5.5,19,6,18.8,6.3L6.3,18.8C6,19,5.5,19,5.2,18.8C5,18.5,5,18,5.2,17.7z"/>
				</svg>
			</button>
			<div class="modal-dialog modal-dialog-centered modal-lg mw-sm-fit-content">
				<div class="modal-content bg-transparent border-0">
					<div class="modal-body p-0">
					</div>
				</div>
			</div>
		</div>`).appendTo(parent)

		$('[data-target="' + selector + '"]').each(function() {
			let title = $(this).find('img').attr('title')
				embed = $(this).data('embed')
				content = `<div class="position-absolute w-100 h-100 d-flex align-items-center justify-content-center bg-black rounded-3"><div class="spinner-border text-white" role="status" style="width:3rem;height:3rem"><span class="sr-only">Loading...</span></div></div><div class="embed-responsive embed-responsive-16by9 rounded-3"><iframe title="` + title + `" class="embed-responsive-item rounded-3" width="` + width + `" height="` + height + `" src="` + embed + `?autoplay=1" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>`
			$(this).attr('data-detail',content)
		})

		$(selector).on('show.bs.modal',function(e) {
			let parent = $(this)
			parent.toggleClass('opacity-0')
			parent.find('.modal-body').html($(e.relatedTarget).data('detail'))
			setTimeout(function() {
				parent.toggleClass('opacity-0')
			},200)
		})

		$(selector).on('hidden.bs.modal',function(e) {
			let parent = $(this)
			parent.find('.modal-body').html('')
		})
	}
